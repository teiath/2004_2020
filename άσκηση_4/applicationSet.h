#ifndef __APPLICATIONSET__
#define __APPLICATIONSET__
#include <set>
#include "officeapp.h"
#include "game.h"


/*
********* ΑΠΟΘΗΚΕΥΣΗ / ΑΝΑΚΛΗΣΗ ΑΠΟ ΤΗΝ ΜΝΗΜΗ ΑΝΤΙΚΕΙΜΕΝΩΝ ΤΗΣ ΚΛΑΣΗΣ Application
*/


// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Application*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s, Application *f);
// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Game*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s, Game *f);
// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Officeapp*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s, Officeapp *f);


// Διαγραφή του αποθηκευμένου στην δομή δεδομένων s αντικειμένου Application*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_del(set<Application *> &s, Application *f);


using ApplicationFtype = bool(const Application a);
// εκτέλεσε την F(x) για όλα τα x στο s
// ή μέχρι η F να επιστρέψει false
void applyApplication(set<Application *> &s, ApplicationFtype & F);

#endif
