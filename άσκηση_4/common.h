// Μερικοί βολικοί ορισμοί

#ifndef __COMMON__
#define __COMMON__


// Οι τύποι αρχείων στο δίσκο που διαχειρίζεται η mad robot
enum DATAFILES {
  DEVELOPERS=0,	// κατασκευαστές εφαρμογών
  GAMES,	// εφαρμογές παιγνιδιών
  OFFICEAPPS,	// εφαρμογές γραφείου
  OFFICETYPES,	// τι τύπους αρχείων υποστηρίζει καθένα από τα OFFICEAPPS
  REVIEWS	// αξιολογήσεις που έχει δεχθεί η κάθε εφαρμογή
};



// ονόματα αρχείων τύπου CSV προς ανάγνωση (αρχεία εισόδου)
#define INP_developers	"input_data/DEVELOPERS_inp.csv"
#define INP_games	"input_data/GAMES_inp.csv"
#define INP_officeapps	"input_data/OFFICEAPPS_inp.csv"
#define INP_officetypes	"input_data/OFFICETYPES_inp.csv"
#define INP_reviews	"input_data/REVIEWS_inp.csv"

// ονόματα αρχείων τύπου CSV προς εγγραφή (αρχεία εξόδου)
#define OUT_developers	"DEVELOPERS_out.csv"
#define OUT_games	"GAMES_out.csv"
#define OUT_officeapps	"OFFICEAPPS_out.csv"
#define OUT_officetypes	"OFFICETYPES_out.csv"
#define OUT_reviews	"REVIEWS_out.csv"


// διαχωριστής πεδίων σε αρχείο CSV
#define CSV_delimiter	'|'		// char

#endif
