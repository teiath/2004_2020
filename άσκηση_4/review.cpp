#include "review.h"
#include "common.h"
#include <iostream>

/*
********* ΔΙΑΧΕΙΡΙΣΗ ΑΝΤΙΚΕΙΜΕΝΩΝ ΤΗΣ ΚΛΑΣΗΣ Review
*/

int Review::get_stars()const { return stars; }
void Review::set_stars(const int s) { stars = s; };
string Review::get_name()const { return name; }
void Review::set_name(const string s) { name = s; };
string Review::get_comment()const { return comment; }
void Review::set_comment(const string s) { comment = s; };

Review::Review() {
  stars = 0;
  name = "";
  comment = "";
}

Review::Review(const Review &r) {
  stars = r.stars;
  name = r.name;
  comment = r.comment;
}

Review::Review(const int s, const string n, const string c) {
  stars = s;
  name = n;
  comment = c;
};

Review  & Review::operator=(const Review& r) {
  if (this == &r) return *this;

  stars = r.stars;
  name = r.name;
  comment = r.comment;
  return *this;
}

bool Review::operator==(const Review& r)const {
  return (
           stars == r.stars &&
           name == r.name &&
           comment == r.comment);
}

bool Review::operator<(const Review& r)const {
  // ταξινόμηση βάσει ονόματος
  return (name < r.name);
}


ostream & operator<<(ostream &o, const Review& r) {
  o << r.stars << CSV_delimiter << r.name << CSV_delimiter << r.comment;
  return o;
}



#ifdef TEST_UNIT

int main () {

  cout  << "Δοκιμή αρχικοποίησης της αξιολόγησης r1:" << endl;
  Review r1(5, "όνομα_1", "σχόλιο_1");
  cout << "r1: " <<  r1 << endl;
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης της r2 με αντιγραφή της r1:" << endl;
  Review r2(r1);
  cout << "r2: " <<  r2 << endl;
  cout << endl;

  cout  << "Δοκιμή εκχώρησης της r1 στην r3:" << endl; 
  Review r3 = r1;
  cout << "r3: " << r3 << endl;
  cout << endl;

  cout << "r1: " << r1 << endl;
  cout << "r3: " << r3 << endl;
  cout  << "Δοκιμή σύγκρισης της r1 με την r3:" << endl; 
  cout << "r1 == r3: " << (r1 == r3) << endl;
  cout << "r1  < r3: " << (r1  < r3) << endl;
  cout << endl;

  cout << "r1: " << r1 << endl;
  Review r4(2, "όνομα_4", "σχόλιο_4");
  cout << "r4: " << r4 << endl;
  cout  << "Δοκιμή σύγκρισης της r1 με την r4:" << endl; 
  cout << "r1 == r4: " << (r1 == r4) << endl;
  cout << "r1  < r4: " << (r1  < r4) << endl;
  cout << endl;
}


#endif				//TEST_UNIT
