#ifndef __DEVELOPERSET__
#define __DEVELOPERSET__
#include "developer.h"


/*
********* ΑΠΟΘΗΚΕΥΣΗ / ΑΝΑΚΛΗΣΗ ΑΠΟ ΤΗΝ ΜΝΗΜΗ ΑΝΤΙΚΕΙΜΕΝΩΝ ΤΗΣ ΚΛΑΣΗΣ Developer
*/


// Προσθήκη στην δομή δεδομένων της μνήμης ενός αντικειμένου τύπου Developer
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool DeveloperSet_add(set<Developer> &S, const Developer &r);

// Διαγραφή του αποθηκευμένου στην δομή δεδομένων της μνήμης Developer
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool DeveloperSet_del(set<Developer> &S, const Developer &r);

#endif
