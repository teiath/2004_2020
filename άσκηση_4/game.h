#ifndef __GAME__
#define __GAME__

#include "application.h"

class Game: public Application {

private:
  bool online;
  int category;

public:
  bool get_online()const;
  void set_online(const bool o);
  int get_category()const;
  void set_category(const int c);

  Game();
  ~Game();
  Game(const Application &a, const bool o, const int c);
  Game & operator=(const Game &g);

  bool operator==(const Game& g)const;
  bool operator<(const Game& g)const;

  friend ostream & operator<<(ostream &o, const Game &a);

};

#endif
