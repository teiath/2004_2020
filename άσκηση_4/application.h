#ifndef __APPLICATION__
#define __APPLICATION__

#include <string>
#include <iostream>
#include <set>
#include "developer.h"
#include "review.h"
using namespace std;


class Application {

protected:			// όχι private για να κληρονομηθούν
  char * code;
  string name;
  string minversion;
  string developercode;		// κωδικός κατασκευαστή
  set<Review *> reviews;
  float price;

public:
  char * get_code()const;
  void set_code(const char * s);
  string get_name()const;
  void set_name(const string s);
  string get_minversion()const;
  void set_minversion(const string s);
  string get_developercode()const;
  void set_developercode(const string s);
  set<Review *> get_reviews()const;
  void set_reviews(const set<Review *> &r);
  void erase_all_reviews();		// Διαγραφή όλων των αξιολογήσεων
  float get_price()const;
  void set_price(const float s);

  Application();
  virtual ~Application();
  Application(const Application &a);
  Application(const string c, const string n, const string m, const string d, const float p);
  Application & operator=(const Application &a);
  bool operator==(const Application &a)const;
  bool operator<(const Application &a)const;

  Application & operator+=(const Review &r);	// προσθήκη αξιολόγησης
  bool add_review(const Review &r);
  Application & operator-=(const Review &r);	// αφαίρεση αξιολόγησης
  bool delete_review(const Review &r);

  friend ostream & operator<<(ostream &o, const Application &a);


  virtual void dummy() { }			// θέλω πολυμορφισμό!
};

#endif
