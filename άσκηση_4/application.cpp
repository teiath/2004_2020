#include <string.h>		// strlen(), strncpy(), strcmp()
#include <iomanip>		// setprecision()
#include <locale>		// std::locale, std::numpunct, std::use_facet
#include <typeinfo>		// typeid()
#include "commaDecimalPoint.h"	// αλλάζει την τελεία σε υποδιαστολή
#include "application.h"
#include "common.h"


char * Application::get_code()const {
  return code;
}
void Application::set_code(const char * s) {
  char *p = new char[strlen(s)+1];
  strncpy(p, s, strlen(s)+1);
  p[strlen(s)] = (char) 0;
  code = p;
}

string Application::get_name()const {
  return name;
}
void Application::set_name(const string s) {
  name = s;
};
string Application::get_minversion()const {
  return minversion;
}
void Application::set_minversion(const string s) {
  minversion = s;
};
string Application::get_developercode()const {
  return developercode;
}
void Application::set_developercode(const string s) {
  developercode = s;
};
float Application::get_price()const {
  return price;
}

set<Review *> Application::get_reviews()const {
  auto s = new set<Review *>;
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    *tmp = *(*i);
    (*s).insert(tmp);
    i++;
  }
  return *s;
}

// Διαγραφή όλων των αξιολογήσεων
void Application::erase_all_reviews() {
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    delete *i;		// πρώτα σβήσε το σχόλιο
    reviews.erase(i);	// και μετά τον δείκτη προς αυτό
    i++;
  }
}

void Application::set_reviews(const set<Review *> &r) {
  // Διαγραφή τυχόν παλαιών αξιολογήσεων
  erase_all_reviews();

  auto i = r.begin();
  while(i != r.end()) {
    auto tmp = new Review;
    *tmp = *(*i);
    reviews.insert(tmp);
    i++;
  }
}

Application& Application::operator+=(const Review &r) {
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    if (*(*i) == r ) {
      return *this;			// υπάρχει ήδη
    }
    i++;
  }
  auto tmp = new Review;
  *tmp = r;
  reviews.insert(tmp);
  return *this;
}

Application& Application::operator-=(const Review &r) {
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    if (*(*i) == r ) {
      delete *i;		// πρώτα σβήσε το σχόλιο
      reviews.erase(i);	// και μετά τον δείκτη προς αυτό
      break;
    }
    i++;
  }
  return *this;
}

bool Application::add_review(const Review &r) {
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    if (*(*i) == r ) {
      return false;			// υπάρχει ήδη
    }
    i++;
  }
  pair<set<Review *>::iterator,bool> ret;
  Review *tmp = new Review;
  *tmp = r;
  ret = reviews.insert(tmp);
  return ret.second;
}

bool Application::delete_review(const Review &r) {
  auto i = reviews.begin();
  while (i != reviews.end()) {
    auto tmp = new Review;
    if (*(*i) == r ) {
      delete *i;		// πρώτα σβήσε το σχόλιο
      reviews.erase(i);	// και μετά τον δείκτη προς αυτό
      return true;
    }
    i++;
  }
  return false;			// δεν βρέθηκε
}



void Application::set_price(const float s) {
  price = s;
};



Application::Application() {
  code = new char[1];
  *code=(char )0;
  name = "";
  minversion = "";
  developercode = "";
  reviews.clear();
  price = 0.0;
}

Application::~Application() {
  if (code) delete[] code;
}


Application::Application(const Application &a) {
  char *p = new char[strlen(a.code)+1];
  strncpy(p, a.code, strlen(a.code)+1);
  p[strlen(a.code)] = (char) 0;
  code = p;
  name = a.name;
  minversion = a.minversion;
  developercode = a.developercode;

  erase_all_reviews();
  set<Review *> K = a.get_reviews();

  auto it = K.begin();
  pair<set<Review *>::iterator,bool> ret;
  while (it!=K.end()) {
    auto tmp = new Review;
    *tmp = *(*it);
    reviews.insert(tmp);
    it++;
  }

  price = a.price;
}

Application::Application(const string c, const string n, const string m, const string d, const float p) {
  char *q = new char[c.size()+1];
  strncpy(q, c.c_str(), c.size()+1);
  q[c.size()] = (char) 0;
  code = q;
  name = n;
  minversion = m;
  developercode = d;
  erase_all_reviews();
  price = p;
}

Application & Application::operator=(const Application &a) {
  if (this == &a) return *this;

  if (code) delete[] code;

  char *p = new char[strlen(a.code)+1];
  strncpy(p, a.code, strlen(a.code)+1);
  p[strlen(a.code)] = (char) 0;
  code = p;
  name = a.name;
  developercode = a.developercode;
  minversion = a.minversion;

  erase_all_reviews();
  set<Review *> K = a.get_reviews();
  auto it = K.begin();
  while (it!=K.end()) {
    auto tmp = new Review;
    *tmp = *(*it);
    reviews.insert(tmp);
    it++;
  }

  price = a.price;

  return *this;
}


bool Application::operator==(const Application& g)const {
// σύγκριση βάσει κωδικού
  return (string(get_code()) == string(g.get_code()));
}

bool Application::operator<(const Application& g)const {
  // ίδιος κωδικός σημαίνει ισότητα
  if (string(get_code()) == string(g.get_code())) return false;
// σύγκριση βάσει κωδικού
  return (string(get_code()) < string(g.get_code()));
}


string developer_with_code_as_string(string s);
ostream & operator<<(ostream &o, const Application &a) {
  // χρήση του χαρακτήρα ','  για διαχωρισμό δεκαδικών ψηφίων αντί για "."
  o.imbue(std::locale(o.getloc(), new commaDecimalPoint));

  o << a.get_code() << CSV_delimiter << a.get_name()
    << CSV_delimiter << a.get_minversion() << CSV_delimiter
    << fixed << showpoint << setprecision(2) << a.get_price() << endl;



  set<Review *> K = a.get_reviews();

    auto it = K.begin();
    int count=0; double total_stars=0;
    while (it != (K.end())) {
      if (count == 0)
        o << "\tΑξιολογήσεις:" << endl; 
#if 1
      // Ενεργοποιώντας τον παρακάτω κώδικα, μαζί με τα άλλα στοιχεία
      // της εφαρμογής τυπώνονται και οι αξιολογήσεις που έχει δεχθεί
      // αναλυτικά
      o << "\t" << setw(4) << (count+1) << ". " << (*it)->get_stars()
        << "* " << (*it)->get_name()
        <<  ": " << (*it)->get_comment() << endl;
#endif
      total_stars += (*it)->get_stars();
      count++;
      it++;
    }
  if (count == 0) {total_stars = 0; } else {total_stars = total_stars/count;}
  o << "\t" << K.size()
    << ( K.size()==1 ? " αξιολόγηση" : " αξιολογήσεις")
    << "  (μέσος όρος " << fixed << setprecision(2) << total_stars << ")"
    << endl;



  return o;
};




#ifdef TEST_UNIT
string developer_with_code_as_string(string code) {
  string t = code;
  t+='|';
  t +="Software 'n' more";
  t+='|';
  t +=code + "@in.hell";
  return t;
}

int main () {

  Review r1(0, "ΜΑΚΗΣ ΚΑΚΙΟΥΣΗΣ", "η χειρότερη εφαρμογή που γνωρίζω");
  Review r2(5, "ΣΑΚΗΣ ΚΑΛΟΥΣΗΣ", "Φανταστική εφαρμογή!!");

  cout  << "Δοκιμή αρχικοποίησης της εφαρμογής a1:" << endl;
  Application a1("off_app1","Office Deluxe 2", "3.2", "DEV1", 10.0);
  cout << "a1: " <<  a1 << endl;
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης της a2 με αντιγραφή της a1:" << endl;
  Application a2(a1);
  a1.set_developercode("DEV2");
  cout << "a2: " <<  a2 << endl;
  cout << endl;

  cout  << "Δοκιμή εκχώρησης της a1 στην a3:" << endl;
  Application a3 = a1;
  cout << "a3: " << a3 << endl;
  cout << endl;

  cout << "a1: " << a1 << endl;
  cout << "a3: " << a3 << endl;
  cout  << "Δοκιμή σύγκρισης της a1 με την a3:" << endl;
  cout << "a1 == a3: " << (a1 == a3) << endl;
  cout << "a1  < a3: " << (a1  < a3) << endl;
  cout << endl;

  cout << "a1: " << a1 << endl;
  Application a4("off_app4","Office Deluxe 1", "3.1", "DEV01", 8.0);
  a4.set_developercode("DEV04");
  cout << "a4: " << a4 << endl;
  cout  << "Δοκιμή σύγκρισης της a1 με την a4:" << endl;
  cout << "a1 == a4: " << (a1 == a4) << endl;
  cout << "a1  < a4: " << (a1  < a4) << endl;
  cout << endl;


  cout  << "Δοκιμή προσθήκης αξιολογήσεων στην εφαρμογή a4:" << endl;
  a4 += r1;
  cout << "a4: " << a4 << endl;
  a4.add_review(r2);
  cout << "a4: " << a4 << endl;
  cout  << "Δοκιμή διαγραφής αξιολογήσεωναπό την εφαρμογή a4:" << endl;
  a4 -= r1;
  cout << "a4: " << a4 << endl;
  a4.delete_review(r2);
  cout << "a4: " << a4 << endl;

}


#endif				//TEST_UNIT
