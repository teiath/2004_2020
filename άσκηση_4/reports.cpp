// Δημιουργία αναφορών στο κανάλι εξόδου o

#include <iostream>
#include <string>
#include <typeinfo>			// typeid()
#include "exairesi.h"
#include <iomanip>			// setw()
#include "commaDecimalPoint.h"		// αλλάζει την τελεία σε υποδιαστολή
#include "madrobot.h"
#include "developerSet.h"
#include "applicationSet.h"
#include "common.h"
using namespace std;


size_t Madrobot::report_Developer(ostream & o) {
  o << "ΚΑΤΑΣΚΕΥΑΣΤΕΣ ΕΦΑΡΜΟΓΩΝ (κωδικός"<<CSV_delimiter<<"όνομα"<<CSV_delimiter<<"email)" <<endl;
  o << "=============================================" <<endl;
  size_t c = 0;
  auto i = get_setDeveloper().begin();
  while (i != get_setDeveloper().end()) {
    o << setw(4) << ++c << ". " << *i << endl;
    i++;
  }
  o << endl;
  return c;
}


size_t Madrobot::report_Application(ostream & o) {
  o << "ΕΦΑΡΜΟΓΕΣ (κωδικός"<<CSV_delimiter<<"όνομα"<<CSV_delimiter<<"ελάχιστη έκδοση λ.σ."<<CSV_delimiter<<"τιμή)" <<endl;
  o << "===================================================" <<endl;
  size_t c = 0;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    bool found = 0;
    if ( typeid (*(*i)) == typeid(Game)) {
      found = true;
      o << setw(4) << ++c << ". "<< *((Game *)(*i));
    }
    if ( typeid (*(*i)) == typeid(Officeapp)) {
      found = true;
      o << setw(4) << ++c << ". "<< *((Officeapp *)(*i));
    }
    if (found)
      o << "\tΚατασκευαστής: "
        << developer_with_code_as_string(string((*i)->get_developercode()))
        << endl;
    i++;
  }
  o << endl;


  return c;
}


size_t Madrobot::report_Game(ostream & o) {
  o << "ΠΑΙΓΝΙΔΙΑ (κωδικός"<<CSV_delimiter<<"όνομα"<<CSV_delimiter<<"ελάχιστη έκδοση λ.σ."<<CSV_delimiter<<"τιμή)" <<endl;
  o << "===================================================" <<endl;
  size_t c = 0;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( typeid (*(*i)) == typeid(Game)) {
      o << setw(4) << ++c << ". "<< *((Game *)(*i));
      o << "\tΚατασκευαστής: "
        << developer_with_code_as_string(string((*i)->get_developercode()))
        << endl;
    }
    i++;
  }
  o << endl;
  return c;
}

size_t Madrobot::report_Officeapp(ostream & o) {
  o << "ΕΦΑΡΜΟΓΕΣ ΓΡΑΦΕΙΟΥ (κωδικός"<<CSV_delimiter<<"όνομα"<<CSV_delimiter<<"ελάχιστη έκδοση λ.σ."<<CSV_delimiter<<"τιμή)" <<endl;
  o << "============================================================" <<endl;
  size_t c = 0;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( typeid (*(*i)) == typeid(Officeapp)) {
      o << setw(4) << ++c << ". " << *((Officeapp *)(*i));
      o << "\tΚατασκευαστής: "
        << developer_with_code_as_string(string((*i)->get_developercode()))
        << endl;
    }
    i++;
  }
  o << endl;
  return c;
}

void Madrobot::report_full(ostream & o) {
  size_t dc, gc, oc, rc;
  dc = report_Developer(o);
  gc = report_Game(o);
  oc = report_Officeapp(o);

  rc = 0;
  auto dd = get_setApplication().begin();
  while (dd != get_setApplication().end()) {
    rc +=  (*dd)->get_reviews().size();
    dd++;
  }

  o << "ΣΤΑΤΙΣΤΙΚΑ" << endl;
  o << "==========" << endl;

  o << "  Κατασκευαστές         : " << dc << endl;
  o << "  Παιγνίδια             : " << gc << endl;
  o << "  Εφαρμογές γραφείου    : " << oc << endl;
  o << "  Αξιολογήσεις          : " << rc << endl;
  o << endl;

}




size_t Madrobot::report_Review(ostream &o) {
  return 0;
}

// ποιες είναι οι δωρεάν εφαρμογές γραφείου;
size_t Madrobot::report_Officeapp_zero_price(ostream &o) {
  o << "ΔΩΡΕΑΝ ΕΦΑΡΜΟΓΕΣ ΓΡΑΦΕΙΟΥ" << endl;
  o << "=========================" << endl;

  size_t c = 0;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( ( typeid (*(*i)) == typeid(Officeapp)) &&
         ((*(*i)).get_price() == 0.0 )) {
      o << setw(4) << (c+1) << ".  "
        << *(*i);
      o << "\tΚατασκευαστής: "
        << developer_with_code_as_string(string((*i)->get_developercode()))
        << endl;
      c++;
    }
    i++;
  }
  o << endl;
  return c;
}


// ποια παιγνίδια έχουν μέσο όρο αξιολόγησης > 4;
size_t Madrobot::report_Game_above_four_stars(ostream &o) {
  o << "ΠΑΙΓΝΙΔΙΑ ΜΕ ΜΕΣΟ ΟΡΟ ΑΞΙΟΛΟΓΗΣΗΣ >4" << endl;
  o << "====================================" << endl;

  size_t c = 0;
  size_t nreviews;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( typeid (*(*i)) == typeid(Game)) {

      set<Review *> sr = (*(*i)).get_reviews();
      float stars_average = 0;
      nreviews = sr.size();
      auto ir = sr.begin();
      while (ir != sr.end()) {
        stars_average += (*ir)->get_stars();
        ir++;
      }

      if (nreviews == 0) {
        stars_average = 0;
      } else {
        stars_average = stars_average / nreviews;
      }
      if (stars_average > 4) {
        o << setw(4) << (c+1) << ".  "
          << *(*i);
        o << "\tΚατασκευαστής: "
          << developer_with_code_as_string(string((*i)->get_developercode()))
          << endl;
        c++;
      }
    }
    i++;
  }
  o << endl;
  return c;
}
