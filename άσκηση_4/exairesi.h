// Κλάση διαχείρισης εξαιρέσεων

#ifndef __EXAIRESI__
#define __EXAIRESI__


#include <string>
using namespace std;

class Exairesi {
public:
  Exairesi();
  ~Exairesi();
  Exairesi(const string s);
  Exairesi(const int i);
};


// Αν το όρισμα δεν είναι ακέραιος ή string,
// εκτελείται ο γενικός κατασκευαστής
#define CATCH 	\
		/* γνωστή εξαίρεση; */ 	\
	catch (Exairesi &e) { } 	\
		/* γενική εξαίρεση; */ 	\
	catch (...) { Exairesi(); }
// γνωστή εξαίρεση;


#define ERROR(x)	try {throw Exairesi(x);} CATCH

#endif
