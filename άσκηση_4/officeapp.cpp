#include <string.h>		// strcmp()
#include <iomanip>		// setprecision()
#include "common.h"
#include "commaDecimalPoint.h"	// αλλάζει την τελεία σε υποδιαστολή
#include "officeapp.h"
using namespace std;

set<string>& Officeapp::get_types()const {
  auto tmp = new set<string>;
  *tmp = types;
  return *tmp;
}

void Officeapp::set_types(const set<string> t) {
  types = t;
}

bool Officeapp::add_types(const string t) {
  pair<std::set<string>::iterator,bool> ret;
  ret = types.insert(t);
  return ret.second;
}

bool Officeapp::del_types(const string t) {
  return(types.erase(t) == 1);
}

Officeapp::Officeapp():Application() {
  types.clear();
}

Officeapp::~Officeapp() {
  types.clear();
};

Officeapp::Officeapp(const Application &a, const set<string> &t)
  :Application(a) {
  types = t;
}


Officeapp & Officeapp::operator=(const Officeapp& a) {
  if (this == &a) return *this;

  if (code) delete[] code;
  types.clear();

  char *p = new char[strlen(a.code)+1];
  strncpy(p, a.code, strlen(a.code)+1);
  p[strlen(a.code)] = (char) 0;
  code = p;
  name = a.name;
  developercode = a.developercode;
  minversion = a.minversion;
  reviews.clear();
  price = a.price;

  types = a.get_types();

  return *this;
}

bool Officeapp::operator==(const Officeapp& g)const {
  // σύγκριση βάσει κωδικού
  return (strcmp(get_code(), g.get_code()) == 0);
}

bool Officeapp::operator<(const Officeapp& g)const {
  // ίδιο κλειδί σημαίνει ισότητα
  if (strcmp(get_code(), g.get_code()) == 0) return false;
  // σύγκριση βάσει κωδικού
  return ((strcmp(get_code(), g.get_code())) < 0);
}

ostream & operator<<(ostream &o, const Officeapp &a) {
  o << (Application) a;

  set<string> S = a.get_types();
  if (S.empty()) {
    return o;
  } else {
    o << "\tΤύποι αρχείων:";
    auto i = S.begin();
    while(i != S.end()) {
      o << " " << *i++;
    }
    o << endl;
  }

  return o;
};

#ifdef TEST_UNIT
Developer developer_with_code(string code) {
  auto d = new Developer;
  d->set_code(code);
  d->set_name("Super Duper Soft 'n' more");
  d->set_email(code + "@in.hell");
  return *d;
}
Developer developer_with_code(string code);


void print(const Officeapp &a) {
  cout << a.get_code() << "|" << a.get_name()
       << '|' << a.get_minversion() <<  '|' << a.get_price() << endl;
  cout << "\tΚατασκευαστής: " << a.get_developercode() << endl;

  set<string> S = a.get_types();
  if (S.empty()) {
    return ;
  } else {
    cout << "\tΤύποι αρχείων:";
    auto i = S.begin();
    while(i != S.end()) {
      cout << " " << *i;
      i++;
    }
    cout << endl;
  }

};


int main () {

  string ftypes[]= {"txt","doc","xls","rtf","odt","docx","xlsx","html"};

  cout  << "Δοκιμή αρχικοποίησης της εφαρμογής o1:" << endl;
  Application a1("off_app1", "Office 2 Deluxe", "2.8", "DEV101", 10.0);
  set <string> types1 (ftypes, ftypes+3);
  Officeapp o1(a1, types1);
  cout << "o1: " ;
  print(o1);
  cout << endl;
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης της o2 με αντιγραφή της o1:" << endl;
  Officeapp o2(o1);
  cout << "o2: " ;
  print(o2);
  cout << endl;
  cout << endl;

  cout  << "Δοκιμή εκχώρησης της o1 στην o3:" << endl;
  Officeapp o3 = o1;
  cout << "o3: " ;
  print(o3);
  cout << endl;
  cout << endl;

  cout << "o1: " ;
  print(o1);
  cout << endl;
  cout << "o3: " ;
  print(o3);
  cout << endl;
  cout  << "Δοκιμή σύγκρισης της o1 με την o3:" << endl;
  cout << "o1 == o3: " << (o1 == o3) << endl;
  cout << "o1  < o3: " << (o1  < o3) << endl;
  cout << endl;

  cout << "o1: " ;
  print(o1);
  cout << endl;
  Application a4("off_app4","Office 1 Deluxe", "1.0", "DEV102", 0.0);
  set <string> types2 (ftypes, ftypes+ sizeof(ftypes)/sizeof(ftypes[0]));
  Officeapp o4(a4, types2);
  cout << endl;

  cout  << "Δοκιμή προσθήκης νέου υποστηριζόμενου τύπου (pdf) στην ο4:"
        << (o4.add_types(string("pdf")) ? "OK" : "απέτυχε") << endl;
  cout << "o4: " ;
  print(o4);
  cout << endl;
  cout << endl;
  cout  << "Δοκιμή προσθήκης υποστηριζόμενου τύπου (pdf) στην ο4 ενώ υπάρχει ήδη:"
        << (o4.add_types(string("pdf")) ? "OK" : "απέτυχε") << endl;
  cout << "o4: " ;
  print(o4);
  cout << endl;
  cout << endl;


  cout << "o1: " ;
  print(o1);
  cout << endl;
  cout << "o4: " ;
  print(o4);
  cout << endl;
  cout  << "Δοκιμή σύγκρισης της o1 με την o4:" << endl;
  cout << "o1 == o4: " << (o1 == o4) << endl;
  cout << "o1  < o4: " << (o1  < o4) << endl;
  cout << endl;

  cout  << "Δοκιμή αντιγραφής της o4 στην o5:" << endl;
  Officeapp o5;
  o5 = o4;
  cout << "o4: " ;
  print(o4);
  cout << endl;
  cout << "o5: " ;
  print(o5);
  cout << endl;

}


#endif				//TEST_UNIT
