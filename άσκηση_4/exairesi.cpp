#include <iostream>
#include <string>
#include "exairesi.h"
using namespace std;

Exairesi::~Exairesi() { }

Exairesi::Exairesi() {
  cerr << "Σφάλμα: γενική εξαίρεση" << endl;
  exit (1);
}

Exairesi::Exairesi(const int i) {
  cerr << "Σφάλμα (κωδικός σφάλματος: " << i << ")" <<
       endl;
#ifndef TEST_UNIT
  exit (1);
#endif
}


Exairesi::Exairesi(const string s) {
  cerr << "Σφάλμα: " << s << endl;
#ifndef TEST_UNIT
  exit (1);
#endif
}

#ifdef TEST_UNIT

int main () {


  cout  << "Δοκιμή εξαίρεσης με κείμενο σφάλματος" << endl;
  try {
    throw Exairesi("Το γρανάζι 272 έσπασε");
  } catch (Exairesi &e) { }
  catch (...) {
    Exairesi();
  }

  cout  << endl << "Δοκιμή εξαίρεσης με αριθμό σφάλματος" << endl;
  try {
    throw Exairesi(124567);
  } catch (Exairesi &e) { }
  catch (...) {
    Exairesi();
  }

  cout  << endl;
  ERROR("Δοκιμή μακροεντολής ERROR");
  try {
    throw Exairesi("Δοκιμή μακροεντολής CATCH");
  }
  CATCH;

  cout  << endl << "Δοκιμή εξαίρεσης με άγνωστο τύπο ορίσματος" << endl;
  try {
    throw 3.14159;
  }
  CATCH;


  cout  << endl << "Κανονικό τέλος προγράμματος" << endl;

}

#endif				//TEST_UNIT
