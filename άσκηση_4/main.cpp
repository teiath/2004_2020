/* ΑΝΤΙΚΕΙΜΕΝΟΣΤΡΕΦΗΣ ΠΡΟΓΡΑΜΜΑΤΙΣΜΟΣ Άσκηση 4

Μεταγλώττιση και θέαση αποτελεσμάτων:

       make && ./a.out | less
*/


#include <iomanip>		// setprecision()
#include <iostream>
using namespace std;

#include "madrobot.h"

void test(int n, string s) {
  cout << "\nΔοκιμή: "<< n << " (" << s << ")\n";
}


int main(int argc, char **argv) {

  int ntests = 0;
  Madrobot m;


  test(++ntests, "Δημιουργία νέου κατασκευαστή/τροποποίηση");
  Developer dev1, dev2, dev3("DEV3", "ΒΑΣΙΛΕΙΟΥ", "vas@mail.com");
  dev1=dev3; 			// εκχώρηση
  dev1.set_code("DEV1");	// αλλαγή στοιχείων
  dev1.set_name("ΚΑΚΟΥΡΟΣ");
  dev1.set_email("kak@mail.com");
  dev2.set_code("DEV2");	// αλλαγή στοιχείων
  dev2.set_name("ΔΗΜΗΤΡΙΟΥ");
  dev2.set_email("dim@mail.com");
  Developer dev4("DEV4", "John White", "jwhite@mail.com");
  Developer dev5("BLCK", "Jim Black", "jblack@newmail.eu");
  cout << "developer dev1: " << dev1 << endl;
  cout << "developer dev2: " << dev2 << endl;
  cout << "developer dev3: " << dev3 << endl;

  test(++ntests, "Εισαγωγή κατασκευαστών στο σύστημα");
  m.addDeveloper(dev1);
  m.addDeveloper(dev2);
  m.addDeveloper(dev3);
  m.addDeveloper(dev4);
  m.addDeveloper(dev5);
  m.addDeveloper("DEV6", "ΒΑΣΙΛΕΙΟΥ", "vas6@mail.com");
  m.addDeveloper("DEV7", "ΔΗΜΟΥ", "dim7@mail.com");
  m.addDeveloper("DEV8", "ΔΗΜΑΣ", "dim8@mail.com");
  m.report_Developer(cout);

  test(++ntests, "Διαγραφή κατασκευαστών από το σύστημα");
  cout << "διαγραφή του κατασκευαστή DEV1: "<<(m.delDeveloper("DEV1")?"OK":"απέτυχε")<<endl;
  cout << "διαγραφή του κατασκευαστή DEV2: "<<(m.delDeveloper("DEV2")?"OK":"απέτυχε")<<endl;
  cout << "διαγραφή (ξανά) του DEV2: "<<(m.delDeveloper("DEV2")?"OK":"απέτυχε")<<endl;
  cout << "(δεν μπορεί να γίνει διαγραφή ενός κατασκευαστή που δεν υπάρχει)"<<endl;
  m.report_Developer(cout);



  test(++ntests, "Δημιουργία νέας εφαρμογής γενικού τύπου/τροποποίηση");
  Application app1("OFF121T","ΣΥΜΒΟΛΑΙΟΓΡΑΦΟΣ","3.0","DEV7",  0.00),
              app2, app3("GAM03T","ΦΙΔΑΚΙ","3.2","DEV0", 12.00);
  Application bad1("OFF666","Mad writer 2.31","3.0","BLCK", 0.00);
  Application bad2("GAM666","Mutant Ninja++","3.1","BLCK", 0.00);
  app2 = app1;			// εκχώρηση
  app2.set_code("OFF122T");	// αλλαγή στοιχείων
  app2.set_name("ΣΥΜΒΟΛΑΙΟΓΡΑΦΟΣ pro");
  app2.set_price(4.99);
  float f = 3.99;
  app2.set_price(f);
  app1.set_developercode("DEV8");	// αλλαγή στοιχείων
  app2.set_developercode("DEV4");
  app3.set_developercode("DEV3");
  cout << app1;
  cout << app2;
  cout << app3;

  test(++ntests, "Εισαγωγή εφαρμογών στο σύστημα");
  Application ag1("GAM07T", "Terminal disease II", "1.2", "DEV3", 4.50);
  Application ag2("GAM08T", "Terminal disease III", "1.2", "DEV3", 9.90);
  Game g1(ag1, true, 1),g2(ag2, false, 4), g3(app3,false, 2);
  Game g4(bad2, 1, 1);
  set<string> t = {"TXT", "DOC"};
  Officeapp o1(app1, t),o2(app2,t);
  Officeapp o3(bad1, t);
  m.addGame(g1); m.addGame(g2); m.addGame(g3); m.addGame(g4);
  m.addOfficeapp(o1); m.addOfficeapp(o2); m.addOfficeapp(o3);
  m.addOfficetype("OFF122T", "XLS");	// προσθαφαίρεση υποστηριζομένων τύπων
  m.addOfficetype("OFF122T", "PDF");
  m.delOfficetype("OFF122T", "DOC");
  m.addOfficetype("bad1", "DOCX");
  m.report_Application(cout);



  test(++ntests, "Διαγραφή εφαρρμογών από το σύστημα");
  cout << "διαγραφή της εφαρμογής GAM07T: "<<(m.delApplication("GAM07T")?"OK":"απέτυχε")<<endl;
  cout << "διαγραφή (ξανά) της GAM07T: "<<(m.delApplication("GAM07T")?"OK":"απέτυχε")<<endl;
  cout << "(δεν μπορεί να γίνει διαγραφή μιας εφαρμογής που δεν υπάρχει)"<<endl;
  m.report_Application(cout);


  test(++ntests, "Διαγραφή όλων των εφαρμογών κατασκευαστή που κρίθηκε 'κακόβουλος'");
  cout << "διαγραφή όλων των εφαρμογών του κατασκευαστή BLCK: "<<(m.delApplication_made_by("BLCK")?"OK":"απέτυχε")<<endl;
  cout << "διαγραφή (ξανά) των εφαρμογών του κατασκευαστή BLCK: "<<(m.delApplication_made_by("BLCK")?"OK":"απέτυχε")<<endl;
  cout << "(δεν υπάρχουν πλέον εφαρμογές του ιδίου κατασκευαστή για να διαγραφούν)"<<endl;
  m.report_Application(cout);



  test(++ntests, "Προσθήκη αξιολογήσεων");
  Review r1(0, "ΜΑΚΗΣ ΚΑΚΙΟΥΣΗΣ", "η χειρότερη εφαρμογή που γνωρίζω");
  Review r2(5, "ΣΑΚΗΣ ΚΑΛΟΥΣΗΣ", "Φανταστική εφαρμογή!!");
  m.addReview("GAM03T", r1);
  m.addReview("GAM03T", r2);
  m.addReview("OFF121T", r2);
  m.report_Application(cout);


  test(++ntests, "Διαγραφή αξιολογήσεων");
  cout << "διαγραφή της αξιολόγησης για την εφαρμογή GAM03T από τον ΜΑΚΗ ΚΑΚΙΟΥΣΗ: "<<(m.delReview("GAM03T","0","ΜΑΚΗΣ ΚΑΚΙΟΥΣΗΣ","η χειρότερη εφαρμογή που γνωρίζω")?"OK":"απέτυχε")<<endl;
  cout << "διαγραφή (ξανά) της για την εφαρμογή GAM03T από τον ΜΑΚΗ ΚΑΚΙΟΥΣΗ: "<<(m.delReview("GAM03T",r1)?"OK":"απέτυχε")<<endl;
  cout << "(δεν μπορεί να γίνει διαγραφή μιας αξιολόγησης που δεν υπάρχει)"<<endl;
  m.report_Application(cout);


  test(++ntests, ("Εύρεση δωρεάν εφαρμογών γραφείου και\nπαιγνιδιών που έχουν μέσο όρο αξιολόγησης > του 4"));
  m.report_Officeapp_zero_price(cout);
  m.report_Game_above_four_stars(cout);


  test(++ntests, ("Πλήρης αναφορά"));
  m.report_full(cout);



  test(++ntests, ("Ανάκτηση/αποθήκευση δεδομένων από/σε αρχεία CSV"));
  cout <<  m.read_files();
  cout <<  m.write_files();


  test(++ntests, ("Εύρεση δωρεάν εφαρμογών γραφείου και\nπαιγνιδιών που έχουν μέσο όρο αξιολόγησης > του 4"));
  m.report_Officeapp_zero_price(cout);
  m.report_Game_above_four_stars(cout);


  test(++ntests, ("Πλήρης αναφορά"));
  m.report_full(cout);

  return 0;
}

