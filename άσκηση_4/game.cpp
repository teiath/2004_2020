#include <iomanip>		// setprecision()
#include <string.h>		// strlen()
#include "common.h"
#include "commaDecimalPoint.h"	// αλλάζει την τελεία σε υποδιαστολή
#include "game.h"
using namespace std;

bool Game::get_online()const {
  return online;
}
void Game::set_online(const bool o) {
  online = o;
}
int Game::get_category()const {
  return category;
}
void Game::set_category(const int c) {
  category = c;
}

Game::Game():Application() {
  online=false;
  category=1;
}
Game::~Game() {
	// δεν χρειάζεται να γίνει κάτι
};
Game::Game(const Application &a, const bool o, const int c)
  :Application(a) {
  online=o;
  category=c;
}


Game & Game::operator=(const Game& a){
  if (this == &a) return *this;

  if (code) delete[] code;

  char *p = new char[strlen(a.code)+1];
  strncpy(p, a.code, strlen(a.code)+1);
  p[strlen(a.code)] = (char) 0;
  code = p;
  name = a.name;
  minversion = a.minversion;
  developercode = a.developercode;
  reviews.clear();
  price = a.price;

  online = a.online;
  category = a.category;
  
  return *this;
}

bool Game::operator==(const Game& g)const{
// σύγκριση βάσει κωδικού
  return (strcmp(get_code(), g.get_code()) == 0);
}

bool Game::operator<(const Game& g)const{
  // ίδιο κλειδί σημαίνει ισότητα
  if (strcmp(get_code(), g.get_code()) == 0) return false;
// σύγκριση βάσει κωδικού
  return ((strcmp(get_code(), g.get_code())) < 0);
}


ostream & operator<<(ostream &o, const Game &a){
  o << (Application) a;

  o << "\tOnline: "; o << (a.online?"ΝΑΙ":"ΟΧΙ");
  o << "\tΚατηγορία: "; o << a.category << endl;

  return o;
};


#ifdef TEST_UNIT
Developer developer_with_code(string code) {
  auto d = new Developer;
  d->set_code(code);
  d->set_name("Super Duper Soft 'n' more");
  d->set_email(code + "@in.hell");
  return *d;
}
Developer developer_with_code(string code);


void print(const Game &a) {
  cout << a.get_code() << "|" << a.get_name()
    << '|' << a.get_minversion() <<  '|' << a.get_price() << endl;
  cout << "\tΚατασκευαστής: " << a.get_developercode() << endl;
};



int main () {

  cout  << "Δοκιμή αρχικοποίησης του παιγνιδιού g1:" << endl;
  Application a1("GAM01","Mortal danger 3", "1.0", "SX81", 9.99);
  Game g1(a1, true, 1);
  cout << "g1: " ; print(g1); cout << endl;
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης του g2 με αντιγραφή του g1:" << endl;
  Game g2(g1);
  cout << "g2: " ; print(g2); cout << endl;
  cout << endl;

  cout  << "Δοκιμή εκχώρησης του g1 στο g3:" << endl; 
  Game g3 = g1;
  cout << "g3: " ; print(g3); cout << endl;
  cout << endl;

  cout << "g1: " ; print(g1); cout << endl;
  cout << "g3: " ; print(g3); cout << endl;
  cout  << "Δοκιμή σύγκρισης του g1 με το g3:" << endl; 
  cout << "g1 == g3: " << (g1 == g3) << endl;
  cout << "g1  < g3: " << (g1  < g3) << endl;
  cout << endl;

  cout << "g1: " ; print(g1); cout << endl;
  Application a4("GAM04","Λαβύρινθος", "5.2", "SX84", 4.0);
  Game g4(a4, false, 3);
  cout << "g4: " ; print(g4); cout << endl;
  cout  << "Δοκιμή σύγκρισης του g1 με το g4:" << endl; 
  cout << "g1 == g4: " << (g1 == g4) << endl;
  cout << "g1  < g4: " << (g1  < g4) << endl;
  cout << endl;
}


#endif				//TEST_UNIT
