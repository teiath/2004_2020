// Αποθήκευση δεδομένων εφαρμογής από την μνήμη σε αρχεία CSV

#include <fstream>
#include <string>
#include <typeinfo>			// typeid()
#include "exairesi.h"
#include "write_files.h"
using namespace std;


// Διαβάζει όλα τα αρχεία, επιστρέφει ένα string με μερικά στατιστικά
string Madrobot::write_files() {
  string info;


  /* ΚΑΤΑΣΚΕΥΑΣΤΕΣ ΕΦΑΡΜΟΓΩΝ */
  info +=
    infoMsg(1, OUT_developers,
            write_csv_file(DEVELOPERS, OUT_developers,
                           "developer_code|developer_name|developer_email",
                           CSV_delimiter));

  /* ΕΦΑΡΜΟΓΕΣ ΠΑΙΓΝΙΔΙΩΝ */
  info +=
    infoMsg(1, OUT_games,
            write_csv_file(GAMES, OUT_games,
                           "application_code|application_name|application_minversion|developer_code|application_price|game_online|game_category",
                           CSV_delimiter));

  /* ΕΦΑΡΜΟΓΕΣ ΓΡΑΦΕΙΟΥ */
  info +=
    infoMsg(1, OUT_officeapps,
            write_csv_file(OFFICEAPPS, OUT_officeapps,
                           "application_code|application_name|application_minversion|developer_code|application_price",
                           CSV_delimiter));

  /* ΠΟΙΟΥΣ ΤΥΠΟΥΣ ΑΡΧΕΙΩΝ ΥΠΟΣΤΗΡΙΖΕΙ Η ΚΑΘΕ ΕΦΑΡΜΟΓΗ ΓΡΑΦΕΙΟΥ */
  info +=
    infoMsg(1, OUT_officetypes,
            write_csv_file(OFFICETYPES, OUT_officetypes,
                           "application_code|filetype_name", CSV_delimiter));

  /* ΑΞΙΟΛΟΓΗΣΕΙΣ */
  info +=
    infoMsg(1, OUT_reviews,
            write_csv_file(REVIEWS, OUT_reviews,
                           "application_code|review_stars|reviewer_name|review_comment",
                           CSV_delimiter));

  return info;
}





#include <iostream>
#include <fstream>
#include <string>
#include "exairesi.h"
using namespace std;


// Γράψε δεδομένα (τύπου string) σε ένα αρχείο (με όνομα filename).

// Αν η παράμετρος header_line δεν είναι το κενό string, γράφεται
// ως πρώτη γραμμή (επικεφαλίδα)

// Επιστρέφει τον αριθμό των γραμμών που γράφηκαν επιτυχώς
// (χωρίς την τυχόν επικεφαλίδα)


long long int Madrobot::write_csv_file(int action, string filename,
                                       string header_line,
                                       char out_delimiter) {

  long long int lines_written_so_far = 0;
  ofstream outfile;

  try {
    outfile.open(filename);
    if (!(outfile.good())) {
      throw Exairesi("Αποτυχία ανοίγματος αρχείου (" + filename +")");
    }

    if (header_line != "") {
      outfile << header_line << endl;
      if (!(outfile.good())) {
        throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
      }
    }


    // Επεξεργασία των πεδίων της γραμμής που μόλις διαβάστηκαν
    bool write_successful;

    write_successful = true;
    switch ( action ) {

    case DEVELOPERS: {
      write_successful = true;
      auto dd = get_setDeveloper().begin();
      while ((dd != get_setDeveloper().end()) && write_successful) {
        outfile << dd->get_code() << CSV_delimiter << dd->get_name()
                << CSV_delimiter << dd->get_email() << endl;
        write_successful = outfile.good();
        if (!(write_successful)) {
          throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
          break;
        } else {
          lines_written_so_far++;
        }
        dd++;
      }
    }
    break;

    case GAMES: {
      write_successful = true;
      auto dd = get_setApplication().begin();
      while ((dd != get_setApplication().end()) && write_successful) {
        if ( typeid (*(*dd)) == typeid(Game)) {
          outfile << (*dd)->get_code() << CSV_delimiter << (*dd)->get_name()
                  << CSV_delimiter << (*dd)->get_minversion()
                  << CSV_delimiter << (*dd)->get_developercode()
                  << CSV_delimiter << (*dd)->get_price()
                  << CSV_delimiter << ((Game *) (*dd))->get_online()
                  << CSV_delimiter << ((Game *) (*dd))->get_category()
                  << endl;
          write_successful = outfile.good();
          if (!(write_successful)) {
            throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
            break;
          } else {
            lines_written_so_far++;
          }
        }
        dd++;
      }
    }
    break;

    case OFFICEAPPS: {
      write_successful = true;
      auto dd = get_setApplication().begin();
      while ((dd != get_setApplication().end()) && write_successful) {
        if ( typeid (*(*dd)) == typeid(Officeapp)) {
          outfile << (*dd)->get_code() << CSV_delimiter << (*dd)->get_name()
                  << CSV_delimiter << (*dd)->get_minversion()
                  << CSV_delimiter << (*dd)->get_developercode()
                  << CSV_delimiter << (*dd)->get_price() << endl;
          write_successful = outfile.good();
          if (!(write_successful)) {
            throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
            break;
          } else {
            lines_written_so_far++;
          }
        }
        dd++;
      }
    }
    break;

    case OFFICETYPES: {
      write_successful = true;
      auto dd = get_setApplication().begin();
      while ((dd != get_setApplication().end()) && write_successful) {
        if ( typeid (*(*dd)) == typeid(Officeapp)) {
          Officeapp *o = (Officeapp *) *dd;
          auto tmp = new set<string>;
          *tmp = o->get_types();
          auto si = (*tmp).begin();
          while ((si != (*tmp).end()) && write_successful) {
            outfile << (*dd)->get_code() << CSV_delimiter << (*si);
            write_successful = outfile.good();
            if (!(write_successful)) {
              throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
              break;
            } else {
              lines_written_so_far++;
            }
            si++;
          }
        }
        dd++;
      }
    }
    break;

    case REVIEWS: {
      write_successful = true;
      auto i = get_setApplication().begin();
      write_successful = true;
      while ((i != get_setApplication().end()) && write_successful) {

        Review *t;
        set<Review *> sr = (*i)->get_reviews();

        auto ir = sr.begin();
        while (ir != sr.end()) {
          outfile << (*i)->get_code() << CSV_delimiter
                  << (*ir)->get_stars() << CSV_delimiter << (*ir)->get_name()
                  << CSV_delimiter << (*ir)->get_comment() << endl;
          write_successful = outfile.good();
          if (!(write_successful)) {
            throw Exairesi("Αποτυχία εγγραφής στο αρχείο (" + filename +")");
            break;
          } else {
            lines_written_so_far++;
          }
          ir++;
        }

        i++;
      }
    }
    break;

    default:
      throw Exairesi("Κλήση της write_csv_file με άγνωστο τύπο ενέργειας!");
    }



    outfile.close();
    if (!(outfile.good())) {
      throw Exairesi("Αποτυχία κλεισίματος αρχείου (" + filename +")");
    }

  }
  CATCH;

  return (lines_written_so_far);
}
