#ifndef __REVIEW__
#define __REVIEW__

#include <string>
#include <set>
using namespace std;

class Review {
private:
  int stars;
  string name;
  string comment;

public:
  int get_stars()const;
  void set_stars(const int s);
  string get_name()const;
  void set_name(const string s);
  string get_comment()const;
  void set_comment(const string s);

  Review();
  Review(const Review &r);
  Review(const int s, const string n, const string c);
  Review& operator=(const Review& r);
  bool operator==(const Review& r)const;
  bool operator<(const Review& r)const;
  friend ostream & operator<<(ostream &o, const Review& r);

};
#define VALID_STARS(s)	(((s) >= 0)||((s)<=5))	// έγκυρος αριθμός αστεριών;

#endif
