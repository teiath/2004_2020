#ifndef __COMMADECIMALPOINT__
#define __COMMADECIMALPOINT__

// μία μίνι κλάση για να αλλάζει το δεκαδικό σημείο στις εκτυπώσεις
// προς ένα κανάλι εξόδου από τελεία σε υποδιαστολή

// τρόπος χρήσης:
// ostream.imbue(std::locale(ostream.getloc(), new commaDecimalPoint));

#include <locale>

class commaDecimalPoint: public std::numpunct<char> {
protected:
  char do_decimal_point() const {
    return ',';
  }
};

#endif
