#include <set>
#include "applicationSet.h"
using namespace std;


// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Application*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s,
                        Application *f) {
  pair<std::set<Application*>::iterator,bool> ret;
  auto tmp = new Application*;
  *tmp = f;
  // Υπάρχει περίπτωση να αντικαθιστούμε με την f εφαρμογή
  // που ήδη υπάρχει, έχοντας αλλάξει κάποια δευτερεύοντα στοιχεία
  // αλλά με τον ίδιο κωδικό. Σε αυτή την περίπτωση η insert() που
  // ακολουθεί θα αποτύγχανε, γι' αυτό διαγράφουμε καλού κακού την
  // f πρώτα, αγνοώντας το αποτέλεσμα της διαγραφής (αφού μπορεί να
  // μην υπάρχει "ίση" εφαρμογή)
  s.erase(*tmp);
  ret = s.insert(*tmp);

  return ret.second;
};


#include <typeinfo>
// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Game*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s,
                        Game *f) {
  pair<std::set<Application*>::iterator,bool> ret;
  auto tmp = new Game*;
  *tmp = f;
  // Υπάρχει περίπτωση να αντικαθιστούμε με την f εφαρμογή
  // που ήδη υπάρχει, έχοντας αλλάξει κάποια δευτερεύοντα στοιχεία
  // αλλά με τον ίδιο κωδικό. Σε αυτή την περίπτωση η insert() που
  // ακολουθεί θα αποτύγχανε, γι' αυτό διαγράφουμε καλού κακού την
  // f πρώτα, αγνοώντας το αποτέλεσμα της διαγραφής (αφού μπορεί να
  // μην υπάρχει "ίση" εφαρμογή)
  s.erase(*tmp);
  ret = s.insert(*tmp);

  return ret.second;
};


// Προσθήκη στην δομή δεδομένων s ενός αντικειμένου τύπου Officeapp*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_add(set<Application *> &s,
                        Officeapp *f) {
  pair<std::set<Application*>::iterator,bool> ret;
  auto tmp = new Officeapp*;
  *tmp = f;
  // Υπάρχει περίπτωση να αντικαθιστούμε με την f εφαρμογή
  // που ήδη υπάρχει, έχοντας αλλάξει κάποια δευτερεύοντα στοιχεία
  // αλλά με τον ίδιο κωδικό. Σε αυτή την περίπτωση η insert() που
  // ακολουθεί θα αποτύγχανε, γι' αυτό διαγράφουμε καλού κακού την
  // f πρώτα, αγνοώντας το αποτέλεσμα της διαγραφής (αφού μπορεί να
  // μην υπάρχει "ίση" εφαρμογή)
  s.erase(*tmp);
  ret = s.insert(*tmp);
  return ret.second;
};


// Διαγραφή του αποθηκευμένου στην δομή δεδομένων s αντικειμένου Application*
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool ApplicationSet_del(set<Application *> &s, Application *f) {
  auto i = s.begin();
  while (i != s.end()) {
    if (string((*(*i)).get_code()) == string(f->get_code())) {
      // Βρέθηκε δείκτης σε εφαρμογή ίδια (βάσει κωδικού) με την f
      // - διάγραψε πρώτα την παραχωρημένη για την εφαρμογή μνήμη
      delete (*i);
      // - και μετά τον σχετικό δείκτη
      return s.erase(*i);
    }
    i++;
  }
  return false;			// δεν βρέθηκε
};


using ApplicationFtype = bool(const Application a);
// εκτέλεσε την F(x) για όλα τα x στο s
// ή μέχρι η F να επιστρέψει false
void applyApplication(set<Application *> &s, ApplicationFtype &F) {
  auto i=s.begin();
  Application* tmp;
  while(i != s.end()) {
    tmp = *i;
    if (! F(*tmp)) break;
    i++;
  }
}




#ifdef TEST_UNIT
#include <typeinfo>			// typeid()
#include "game.h"
#include "officeapp.h"

bool show_set_member(const Application g) {
  cout <<  g ;
  return true;
}

int main () {

  set<Application *> S;
  const set<string> T;

  Application o1("OFF01","Writer Deluxe V", "1.4", 9.99);
  cout << "o1: " << o1;
  cout  << "Δοκιμή προσθήκης o1 στο σύνολο: ";
  cout << (ApplicationSet_add(S, &o1) ? "OK" : "απέτυχε") << endl;
  cout << endl;

  Application o2 = o1;
  cout << "o2: " << o2;
  cout  << "Δοκιμή προσθήκης o2 στο σύνολο: ";
  cout << (ApplicationSet_add(S,&o2) ? "OK" : "απέτυχε") << endl;
  cout << "(δεν επιτρέπεται η αποθήκευση της ίδιας εφαρμογής γραφείου)" << endl;
  cout << endl;

  o2.set_code("off02");
  cout << "o2: " << o2;
  cout  << "Δοκιμή προσθήκης o2 στο σύνολο: ";
  cout << (ApplicationSet_add(S,&o2) ? "OK" : "απέτυχε") << endl;
  cout << "(επιτρέπεται όμως η αποθήκευση της ίδιας με διαφορετικό κωδικό)" << endl;
  cout << endl;


  cout  << "Εκτύπωση όλων ("<<S.size()<<") των αποθηκευμένων στοιχείων:"<< endl;
  applyApplication(S, show_set_member);
  cout << endl;

  cout  << "Δοκιμή διαγραφής της εφαρμογής γραφείου #1 από το σύνολο: ";
  cout << (ApplicationSet_del(S, &o1) ? "OK" : "απέτυχε") << endl;
  applyApplication(S, show_set_member);
  cout << endl;

  cout  << "Δοκιμή διαγραφής ξανά της εφαρμογής γραφείου #1 από το σύνολο: ";
  cout << (ApplicationSet_del(S, &o1) ? "OK" : "απέτυχε") << endl;
  cout << "(δεν μπορεί να διαγραφεί ένα σχόλιο που δεν υπάρχει)" << endl;
  applyApplication(S, show_set_member);
  cout << endl;


  Application o3("OFF03","Author's delight", "3.0", 0.99);
  cout << "o3: " << o3;
  cout  << "Δοκιμή προσθήκης o3 στο σύνολο: ";
  cout << (ApplicationSet_add(S,&o3) ? "OK" : "απέτυχε") << endl;
  cout  << "Δοκιμή διαγραφής της o3 από το σύνολο: ";
  cout << (ApplicationSet_del(S,&o3) ? "OK" : "απέτυχε") << endl;
  cout  << "Αλλαγή μερικών στοιχείων του o3 (προσθήκη υποστηριζόμενων τύπων αρχείου)" << endl;
  o3.set_price(1.99);
  cout  << "Εκτύπωση όλων ("<<S.size()<<") των αποθηκευμένων στοιχείων:"<< endl;
  applyApplication(S, show_set_member);
  cout << endl;

  cout << "o3: " << o3;
  cout  << "Δοκιμή προσθήκης o3 στο σύνολο: ";
  cout << (ApplicationSet_add(S,&o3) ? "OK" : "απέτυχε") << endl;
  cout  << "Εκτύπωση όλων ("<<S.size()<<") των αποθηκευμένων στοιχείων:"<< endl;
  applyApplication(S, show_set_member);
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης του παιγνιδιού g1:" << endl;
  Application a1("GAM01","Mortal danger 3", "1.0", 9.99);
  Game g1(a1, true, 1);
  cout << "g1: " <<  g1 << endl;
  cout << endl;
  cout  << "Δοκιμή προσθήκης g1 στο σύνολο: ";
  cout << (ApplicationSet_add(S,&g1) ? "OK" : "απέτυχε") << endl;
  cout  << "Εκτύπωση όλων ("<<S.size()<<") των αποθηκευμένων στοιχείων:"<< endl;
  applyApplication(S, show_set_member);
  cout << endl;

}

#endif				//TEST_UNIT
