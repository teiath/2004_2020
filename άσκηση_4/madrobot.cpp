// Χειρισμός αντικειμένων της madrobot

#include <typeinfo>			// typeid()
#include "madrobot.h"
#include "applicationSet.h"
#include "developerSet.h"
#include "common.h"

set<Application *> & Madrobot::get_setApplication() {
  return setApplication;
}
set<Developer> & Madrobot::get_setDeveloper() {
  return setDeveloper;
}


// μορφοποίηση μηνύματος με στατιστικά
string Madrobot::infoMsg(int read_or_write, string f, long long int n) {
  string info;
  string str = f;
  str.resize (30,' ');
  info += str;
  info += (read_or_write == 0 ? " διαβάστηκαν " : " γράφτηκαν ");
  info += to_string(n);
  info += (n==1) ? " εγγραφή " : " εγγραφές";
  info += (read_or_write == 0 ? " στην " : " από την ");
  info += "μνήμη\n";
  return info;
}





// υπάρχει εφαρμογή με κωδικό s;
bool Madrobot::exists_Application_with_code(const string s) {
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string((*i)->get_code()) == s) {
      return true;
    }
    i++;
  }
  return false;		// δεν βρέθηκε
}





// υπάρχει εφαρμογή γραφείου με κωδικό s;
bool Madrobot::exists_Officeapp_with_code(const string s) {

  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string( (*i)->get_code() ) == s) {
      return ( typeid (*(*i)) == typeid(Officeapp));
    }
    i++;
  }
  return false;		// δεν βρέθηκε
}



// υπάρχει εφαρμογή παιγνιδιού με κωδικό s;
bool Madrobot::exists_Game_with_code(const string s) {
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string( (*i)->get_code() ) == s) {
      return ( typeid (*(*i)) == typeid(Game));
    }
    i++;
  }
  return false;		// δεν βρέθηκε
}




// Ο κατασκευαστής της εφαρμογής με κωδικό s να γίνει ο d
bool Madrobot::setdeveloper_Application_with_code(const string s, string d) {

  // Αν υπάρχει η εφαρμογή s
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string( (*i)->get_code() ) == s) {
      auto tmp = new Application;
      *tmp = *(*i);
      if (!ApplicationSet_del(setApplication, tmp)) return false;
      tmp->set_developercode(d);
      cout << " new office app: "<< *tmp << endl;
      return ApplicationSet_add(setApplication, tmp);
    }
    i++;
  }

  return false;		// η s δεν είναι γνωστή εφαρμογή
}




// Προσθήκη αξιολόγησης r στην εφαρμογή με κωδικό s
bool Madrobot::addreview_Application_with_code(const string s, const Review r) {

  // Υπάρχει αποθηκευμένη εφαρμογή με κωδικό s;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string((*i)->get_code()) == s) {
      auto tmp = new Application;
      *tmp = *(*i);
      if (!ApplicationSet_del(setApplication, tmp)) return false;
      *tmp += r;
      cout << " new  app review!!: "<< *tmp << endl;
      return ApplicationSet_add(setApplication, tmp);
    }
    i++;
  }

  return false;		// η s δεν είναι γνωστή εφαρμογή
}



// Προσθήκη υποστηριζόμενου τύπου αρχείου t στην εφαρμογή γραφείου με κωδικό s
bool Madrobot::addtype_Officeapp_with_code(const string s, const string t) {
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if (string((*i)->get_code()) == s) {
      if ( typeid (*(*i)) != typeid(Officeapp)) {
        return false;
      }

      return  (((Officeapp *)(*i))->add_types(t)) ;
    }
    i++;
  }

  return false;		// δεν βρέθηκε
}



// Υπάρχει κατασκευαστής με κωδικό s;
bool Madrobot::exists_Developer_with_code(const string s) {
  auto i = get_setDeveloper().begin();
  while (i != get_setDeveloper().end()) {
    if (i->get_code() == s) return true;
    i++;
  }
  return false;
}


// ποιος κατασκευαστής έχει κωδικό s;
Developer Madrobot::developer_with_code(const string s) {
  auto t = new Developer;
  t->set_code(s);

  auto i = get_setDeveloper().begin();
  while (i != get_setDeveloper().end()) {
    if (i->get_code() == s) {
      *t = *i;
      break;
    }
    i++;
  }
  return *t;
}


// ποιος κατασκευαστής έχει κωδικό s; (ως string)
string Madrobot::developer_with_code_as_string(const string s) {
  string t = "";
  auto i = get_setDeveloper().begin();
  while (i != get_setDeveloper().end()) {
    if (i->get_code() == s) {
      t+= i->get_code();
      t+='|';
      t+= string(i->get_name());
      t+='|';
      t+= i->get_email();
      break;
    }
    i++;
  }
  return t;
}


// Προσθήκη/διαγραφή αντικειμένων στις δομές δεδομένων του συστήματος

#include <typeinfo>		// typeid()
#include "madrobot.h"
#include "developerSet.h"
#include "applicationSet.h"
#include "common.h"
using namespace std;


/* ΚΑΤΑΣΚΕΥΑΣΤΕΣ ΕΦΑΡΜΟΓΩΝ */
bool Madrobot::addDeveloper(string s1, string s2, string s3) {
  string msg;
  Developer *tmp = new Developer;
  tmp->set_code(s1);
  tmp->set_name(s2.c_str());
  tmp->set_email(s3);
  return DeveloperSet_add(get_setDeveloper(), *tmp);
}

bool Madrobot::addDeveloper(const Developer d) {
  return DeveloperSet_add(get_setDeveloper(), d);
}

bool Madrobot::delDeveloper(string s) {		// διαγραφή βάσει κωδικού
  Developer *tmp = new Developer;
  tmp->set_code(s);
  return DeveloperSet_del(get_setDeveloper(), *tmp);
}




/* ΕΦΑΡΜΟΓΕΣ ΠΑΙΓΝΙΔΙΩΝ */
bool Madrobot::addGame(string s1,string s2, string s3,
                       string s4, string s5, string s6, string s7) {
  string msg;
  Game *tmp = new Game;

  // Υπάρχει ήδη εφαρμογή με κωδικό s1;
  if (exists_Application_with_code(s1)) return false;

  // Υπάρχει κατασκευαστής εφαρμογών με κωδικό s4;
  if (!(exists_Developer_with_code(s4))) return false;

  tmp->set_code(s1.c_str());
  tmp->set_name(s2);
  tmp->set_minversion(s3);
  tmp->set_developercode(s4);

  // αν η τιμή έχει υποδιαστολή, μετάτρεψέ την σε τελεία
  string s = s5;
  size_t x=s.find(",");
  if (x!= string::npos) {
    s = s5.replace(s.find(","),1,".");
  }
  tmp->set_price(stof(s));

  tmp->set_online( (stoi(s6)==0 ? false : true) );
  tmp->set_category(stoi(s7));
  return ApplicationSet_add(get_setApplication(), tmp);
}
bool Madrobot::addGame(Game g){
  Game *tmp = new Game;
  *tmp = g;
  return ApplicationSet_add(get_setApplication(), tmp);
}




/* ΕΦΑΡΜΟΓΕΣ ΓΡΑΦΕΙΟΥ */
bool Madrobot::addOfficeapp(string s1, string s2, string s3,
                            string s4, string s5) {
  string msg;

  // Υπάρχει ήδη εφαρμογή με κωδικό s1;
  if (exists_Application_with_code(s1)) return false;

  // Υπάρχει κατασκευαστής εφαρμογών με κωδικό s4;
  if (!(exists_Developer_with_code(s4))) return false;

  Officeapp *tmp = new Officeapp;
  tmp->set_code(s1.c_str());
  tmp->set_name(s2);
  tmp->set_minversion(s3);
  tmp->set_developercode(s4);

  // αν η τιμή έχει υποδιαστολή, μετάτρεψέ την σε τελεία
  string s = s5;
  size_t x=s.find(",");
  if (x!= string::npos) {
    s = s5.replace(s.find(","),1,".");
  }
  tmp->set_price(stof(s));
  return ApplicationSet_add(get_setApplication(), tmp);
}
bool Madrobot::addOfficeapp(Officeapp o){
  Officeapp *tmp = new Officeapp;
  *tmp = o;
  return ApplicationSet_add(get_setApplication(), tmp);
}




/* ΔΙΑΓΡΑΦΗ ΕΦΑΡΜΟΓΩΝ (οιουδήποτε τύπου) */
bool Madrobot::delApplication(string s) {
  // Υπάρχει εφαρμογή με κωδικό s;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( (*i)->get_code() == s ) {
      auto tmp = new Application;
      tmp->set_code(s.c_str());
      return ApplicationSet_del(get_setApplication(), tmp);
    }
    i++;
  }
  return false;				// δεν βρέθηκε
}
bool Madrobot::delApplication_made_by(string s) {
  bool found = false;
  // Υπάρχει εφαρμογή με κατασκευαστή που έχει κωδικό s;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( (*i)->get_developercode() == s ) {
      // διάγραψε αυτή την εφαρμογή του και ψάξε να βρεις και τυχόν άλλες
      if (!(delApplication( string((*i)->get_code()) ))) return false;
      found = true;
    }
    i++;
  }
  return found;
}


/* ΥΠΟΣΤΗΡΙΖΟΜΕΝΟΙ ΤΥΠΟΙ ΑΡΧΕΙΩΝ ΕΦΑΡΜΟΓΩΝ ΓΡΑΦΕΙΟΥ */
bool Madrobot::addOfficetype(string s1, string s2) {
  string msg;

  // Υπάρχει εφαρμογή γραφείου με κωδικό s1;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( (*i)->get_code() == s1 ) {
      if ( typeid (*(*i)) != typeid(Officeapp)) return false;

      Officeapp *op;	// δες την εφαρμογή που βρέθηκε σαν εφαρμογή γραφείου
      op = (Officeapp *) (*i);

      // πάρε ένα αντίγραφο των ήδη υποστηριζομένων τύπων
      pair<std::set<string>::iterator,bool> ret;
      auto s = new set<string>;
      *s = op->get_types();
      // πρόσθεσε τον νέο τύπο στο τοπικό αντίγραφο
      ret = (*s).insert(s2);
      if (!(ret.second)) return false;
      // αντικατάστησε τους τύπους της εφαρμογής με το τοπικό αντίγραφο
      op->set_types(*s);
      return true;
    }
    i++;
  }
  return false;				// δεν βρέθηκε
}

bool Madrobot::delOfficetype(string s1, string s2) {
  string msg;

  // Υπάρχει εφαρμογή γραφείου με κωδικό s1;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( (*i)->get_code() == s1 ) {
      if ( typeid (*(*i)) != typeid(Officeapp)) return false;

      Officeapp *op;	// δες την εφαρμογή που βρέθηκε σαν εφαρμογή γραφείου
      op = (Officeapp *) (*i);

      // πάρε ένα αντίγραφο των ήδη υποστηριζομένων τύπων
      pair<std::set<string>::iterator,bool> ret;
      auto s = new set<string>;
      *s = op->get_types();
      // αφαίρεσε τον νέο τύπο από το τοπικό αντίγραφο
      if ((*s).erase(s2) != 1) return false;
      // αντικατάστησε τους τύπους της εφαρμογής με το τοπικό αντίγραφο
      op->set_types(*s);
      return true;
    }
    i++;
  }
  return false;				// δεν βρέθηκε
}





/* ΑΞΙΟΛΟΓΗΣΕΙΣ */
bool Madrobot::delReview(string s1,string s2, string s3, string s4) {
  int read_stars = stoi(s2);
  if (!(VALID_STARS(read_stars))) return false;

  // Υπάρχει εφαρμογή με κωδικό s1;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( (*i)->get_code() == s1 ) {
      auto rp = new Review;
      rp->set_stars(read_stars);
      rp->set_name(s3);
      rp->set_comment(s4);
      auto S = new set<Review *>;

      // πάρε ένα τοπικό αντίγραφο των αξιολογήσεων της εφαρμογής
      *S = (*i)->get_reviews();

      bool found = false;
      auto si = (*S).begin();
      while (si != (*S).end()) {
        // σβήσε την αξιολόγηση από το τοπικό αντίγραφο
        if ((*(*si)) == *rp) {
          found = true;
          // Διάγραψε πρώτα την αξιολόγηση
          delete (*si);
          // και μετά και τον δείκτη της
          if (S->erase(*si) != 1) return false;
        }
        si++;
      }
      if (!found) return false;
      // αντικατάστησε τις αξιολογήσεις της εφαρμογής με το τοπικό αντίγραφο
      (*i)->set_reviews(*S);
      return true;
    }
    i++;
  }
  return false;				// δεν βρέθηκε
}

bool Madrobot::addReview(string s1,string s2, string s3, string s4) {
  string msg;
  bool found = false, isGame=false, isOfficeapp=false;
  int read_stars = stoi(s2);
  auto ng = new Game;
  auto no = new Officeapp;

  if (!(VALID_STARS(read_stars))) return false;

  // Υπάρχει η εφαρμογή με κωδικό s1 για την οποία διαβάστηκε αξιολόγηση;
  auto i = get_setApplication().begin();
  while (i != get_setApplication().end()) {
    if ( string((*i)->get_code()) == s1 ) {
      found = true;

      // Ναι, πρόσθεσέ της ένα αντικείμενο review
      auto tmp = new Review;
      tmp->set_stars(read_stars);
      tmp->set_name(s3);
      tmp->set_comment(s4);
      return (*i)->add_review(*tmp);
      break;			// δεν φθάνουμε εδώ ποτέ
    }
    i++;
  }

  return false;		// δεν βρέθηκε σχετική εφαρμογή
}

bool Madrobot::addReview(string s1, Review r){
  return addReview(s1, to_string(r.get_stars()), r.get_name(), r.get_comment());
}

bool Madrobot::delReview(string s1, Review r){
  return delReview(s1, to_string(r.get_stars()), r.get_name(), r.get_comment());
}
