#ifndef __OFFICEAPP__
#define __OFFICEAPP__

#include <set>
#include "application.h"
using namespace std;

class Officeapp: public Application {

private:
  set<string> types;	// οι τύποι αρχείων που μπορεί να χειριστεί η εφαρμογή

public:
  Officeapp();
  ~Officeapp();
  set<string> & get_types()const;
  void set_types(const set<string> t);
  Officeapp(const Application &a, const set<string> &t);
  Officeapp & operator=(const Officeapp &a);

  bool operator==(const Officeapp& o)const;
  bool operator<(const Officeapp& o)const;

  // Προσθήκη αφαίρεση τύπου υποστηριζομένων αρχείων
  bool add_types(const string t);
  bool del_types(const string t);

  friend ostream & operator<<(ostream &o, const Officeapp &a);

};


#endif
