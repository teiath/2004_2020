// Φόρτωμα δεδομένων εφαρμογής στην μνήμη διαβάζοντας αρχεία CSV

#include <fstream>
#include <string>
#include "exairesi.h"
#include "read_files.h"
using namespace std;

#include <typeinfo>
// Διαβάζει όλα τα αρχεία, επιστρέφει ένα string με μερικά στατιστικά
string Madrobot::read_files() {
  string info;

  /* ΚΑΤΑΣΚΕΥΑΣΤΕΣ ΕΦΑΡΜΟΓΩΝ */
  info +=
    infoMsg(0, INP_developers,
            read_csv_file(DEVELOPERS, INP_developers, 3, 1, CSV_delimiter));

  /* ΕΦΑΡΜΟΓΕΣ ΠΑΙΓΝΙΔΙΩΝ */
  info +=
    infoMsg(0, INP_games,
            read_csv_file(GAMES, INP_games, 7, 1, CSV_delimiter));

  /* ΕΦΑΡΜΟΓΕΣ ΓΡΑΦΕΙΟΥ */
  info +=
    infoMsg(0, INP_officeapps,
            read_csv_file(OFFICEAPPS, INP_officeapps, 5, 1, CSV_delimiter));

  /* ΠΟΙΟΥΣ ΤΥΠΟΥΣ ΑΡΧΕΙΩΝ ΥΠΟΣΤΗΡΙΖΕΙ Η ΚΑΘΕ ΕΦΑΡΜΟΓΗ ΓΡΑΦΕΙΟΥ */
  info +=
    infoMsg(0, INP_officetypes,
            read_csv_file(OFFICETYPES, INP_officetypes, 2, 1, CSV_delimiter));

  /* ΑΞΙΟΛΟΓΗΣΕΙΣ */
  info +=
    infoMsg(0, INP_reviews,
            read_csv_file(REVIEWS, INP_reviews, 4, 1, CSV_delimiter));

  return info;
}






// Διάβασε τα πρώτα nfields πεδία από ένα αρχείο CSV με όνομα filename.

// Τα πεδία διαχωρίζονται από τον χαρακτήρα inp_delimiter,
// ενώ αγνοούνται οι πρώτες ignore_lines του αρχείου
// (η πρώτη γραμμή λ.χ. μπορεί να περιέχει τα ονόματα των πεδίων)

// Για κάθε γραμμή που διαβάζεται εκτελείται αντίστοιχη προσθήκη των
// δεδομένων που διαβάστηκαν σε μια δομή δεδομένων μνήμης, η οποία
// επιλέγεται βάσει του ορίσματος action

// Επιστρέφει τον αριθμό των γραμμών δεδομένων που διαβάστηκαν επιτυχώς

// Σε περίπτωση λάθους προκαλεί εξαίρεση


long long int Madrobot::read_csv_file(int action, string filename, int nfields,
                                      int ignore_lines, char inp_delimiter) {

  int fields_found_so_far;

  string fld[nfields];
  long long int lines_read_so_far = 0;

  ifstream infile;
  try {
    infile.open(filename);
    if (!infile.good()) {
      throw Exairesi("Αποτυχία ανοίγματος του αρχείου " + filename);
    }


    string linebuf;
    while(getline(infile, linebuf)) {
      if (!infile.good()) {
        throw Exairesi("Αποτυχία ανάγνωσης από το αρχείο " + filename);
      }


      lines_read_so_far++;
      if (lines_read_so_far > ignore_lines) {

        fields_found_so_far = 0;

        for (int i=0; i<nfields; i++) {
          fld[ i ] = "";
        }
        string part;
        int start_pos = 0;
        while (fields_found_so_far < nfields) {
          // Η γραμμή που διαβάσαμε μπορεί να έχει περισσότερα πεδία
          // από όσα χρειαζόμαστε.  Αγνόησε τα περισσά.

          int delimiter_pos = linebuf.find(inp_delimiter, start_pos);
          if (delimiter_pos < 0) {	// δεν βρέθηκε ο διαχωριστής
            // επομένως το τρέχον πεδίο είναι όλο το υπόλοιπο string
            part = linebuf.substr(start_pos);
            fld[ fields_found_so_far ] = part;
            break;
          }

          part = linebuf.substr(start_pos, (delimiter_pos-start_pos));
          fld[ fields_found_so_far] = part;

          fields_found_so_far++;
          start_pos = delimiter_pos+1;
        }


        // Επεξεργασία των πεδίων της γραμμής που μόλις διαβάστηκαν
        bool read_successful;


        switch ( action ) {

        case DEVELOPERS:
          read_successful = addDeveloper(fld[0],fld[1],fld[2]);
          break;

        case GAMES:
          if (!(exists_Developer_with_code(fld[3]))) {
            string msg = "Δεν υπάρχει κατασκευαστής με κωδικό ";
            msg += fld[3];
            msg += ", δεν μπορεί να προστεθεί ως τέτοιος στην εφαρμογή ";
            msg += fld[0];
            throw Exairesi(msg);
          }
          read_successful = addGame(fld[0],fld[1],fld[2],
                                    fld[3],fld[4],fld[5],fld[6]);
          break;

        case OFFICEAPPS:
          if (!(exists_Developer_with_code(fld[3]))) {
            string msg = "Δεν υπάρχει κατασκευαστής με κωδικό ";
            msg += fld[3];
            msg += ", δεν μπορεί να προστεθεί ως τέτοιος στην εφαρμογή ";
            msg += fld[0];
            throw Exairesi(msg);
          }
          read_successful = addOfficeapp(fld[0],fld[1],fld[2],fld[3],fld[4]);
          break;

        case OFFICETYPES:
          if (!(exists_Application_with_code(fld[0]))) {
            string msg = "Δεν υπάρχει εφαρμογή με κωδικό ";
            msg += fld[0];
            throw Exairesi(msg);
          }
          read_successful = addOfficetype(fld[0],fld[1]);
          break;

        case REVIEWS:
          if (!(exists_Application_with_code(fld[0]))) {
            string msg = "Δεν υπάρχει εφαρμογή με κωδικό ";
            msg += fld[0];
            throw Exairesi(msg);
          }
          read_successful = addReview(fld[0],fld[1],fld[2],fld[3]);
          break;

        default:
          throw Exairesi("Κλήση της read_csv_file με άγνωστο τύπο ενέργειας!");
        }




        if (! read_successful) {
          throw Exairesi("Αποτυχία αποθήκευσης των δεδομένων που διαβάστηκαν από το αρχείο " +
                         filename + " στην γραμμή " + to_string(lines_read_so_far));
        }
      }

    }				// getline
    infile.close();

  }
  CATCH
  // πόσες γραμμές δεδομένων διαβάστηκαν
  return (lines_read_so_far - ignore_lines);
}


