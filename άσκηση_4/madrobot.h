#ifndef __MADROBOT__
#define __MADROBOT__
#include <set>

//
// Κλάσεις για τα αντικείμενα εφαρμογές: Παιγνίδια και Εφαρμογές γραφείου
// (κληρονόμοι της κλάσης Application)
//
#include "game.h"
#include "officeapp.h"

//
// Κλάσεις για δευτερεύοντα αντικείμενα
// (κατασκευαστές, αξιολογήσεις)
//
#include "developer.h"
#include "review.h"


class Madrobot {
//
// Ιδιωτικές δομές δεδομένων
//
private:
  // δομή δεδομένων για εφαρμογές
  set<Application *> setApplication;	// δείκτες σε εφαρμογές για πολυμορφισμό

  // δομή δεδομένων για κατασκευαστές εφαρμογών
  set<Developer> setDeveloper;


  // Για τις παραπάνω δομές επελέγη ο τύπος std::set επειδή διασφαλίζει
  // αυτόματα μοναδικότητα περιεχομένων (ως προς τον τελεστή Application::==)
  // και ταχεία αναζήτηση (σε χρόνο log(N))


//
// Δημόσιες μέθοδοι χειρισμού των αντικειμένων
//
public:
  set<Application *> & get_setApplication();
  set<Developer> & get_setDeveloper();



  // Προσθήκη/διαγραφή δεδομένων στις δομές της μνήμης
  bool addDeveloper(string s1,string s2, string s3);
  bool addDeveloper(Developer d);
  bool delDeveloper(string s);
  bool addGame(string s1,string s2, string s3, string s4, string s5, string s6, string s7);
  bool addGame(Game g);
  bool addOfficeapp(string s1,string s2, string s3, string s4, string s5);
  bool addOfficeapp(Officeapp o);
	// διαγραφή εφαρμογής με κωδικό s
  bool delApplication(string s);
	// διαγραφή όλων των εφαρμογών του κατασκευαστή με κωδικό s
  bool delApplication_made_by(string s);
	// νέος υποστηριζόμενος τύπος s2 για την εφαρμογή γραφείου με κωδικό s1
  bool addOfficetype(string s1, string s2);
  bool delOfficetype(string s1, string s2);
	// νέα αξιολόγηση για την εφαρμογή με κωδικό s1
  bool addReview(string s1,string s2, string s3, string s4);
  bool addReview(string s1, Review r);
  bool delReview(string s1,string s2, string s3, string s4);
  bool delReview(string s1, Review r);



  // Φόρτωμα όλων των δεδομένων της εφαρμογής στην μνήμη από αρχεία CSV
  string read_files();

  // Αποθήκευση όλων των δεδομένων της εφαρμογής από την μνήμη σε αρχεία CSV
  string write_files();

  // Μορφοποίηση μηνύματος με στατιστικά
  string infoMsg(int read_or_write, string f, long long int n);


  // Ανάγνωση δεδομένων από αρχείο CSV
  long long int read_csv_file(int action, string filename, int nfields,
                              int ignore_lines, char inp_delimiter);
  // Εγγραφή δεδομένων σε αρχείο CSV
  long long int write_csv_file(int action, string filename,
                               string header_line, char out_delimiter);


  // Υπάρχει εφαρμογή με κωδικό s;
  bool exists_Application_with_code(const string s);

  // Υπάρχει εφαρμογή γραφείου με κωδικό s;
  bool exists_Officeapp_with_code(const string s);

  // υπάρχει εφαρμογή παιγνιδιού με κωδικό s;
  bool exists_Game_with_code(const string s);

  // Υπάρχει κατασκευαστής με κωδικό s;
  bool exists_Developer_with_code(const string s);
  // ποιος κατασκευαστής έχει κωδικό s;
  Developer developer_with_code(const string s);
  // ποιος κατασκευαστής έχει κωδικό s (όπως παραπάνω, αλλά επιστρέφει string)
  string developer_with_code_as_string(const string s);



  // Προσθήκη υποστηριζόμενου τύπου αρχείου t στην εφαρμογή γραφείου με κωδικό s
  bool addtype_Officeapp_with_code(const string s, const string t);

  // Προσθήκη αξιολόγησης r στην εφαρμογή με κωδικό s
  bool addreview_Application_with_code(const string s, const Review r);

  // Ο κατασκευαστής της εφαρμογής με κωδικό s να γίνει ο d
  bool setdeveloper_Application_with_code(const string s, string d);


// Δημιουργία αναφορών στο κανάλι εξόδου o
  size_t report_Developer(ostream & o);
  size_t report_Application(ostream & o);
  size_t report_Game(ostream & o);
  size_t report_Officeapp(ostream & o);
  size_t report_Review(ostream & o);
  void   report_full(ostream & o);


// ποιες είναι οι δωρεάν εφαρμογές γραφείου;
  size_t report_Officeapp_zero_price(ostream &o);

// ποια παιγνίδια έχουν μέσο όρο αξιολόγησης > 4;
  size_t report_Game_above_four_stars(ostream &o);

};


#endif
