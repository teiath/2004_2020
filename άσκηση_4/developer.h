#ifndef __DEVELOPER__
#define __DEVELOPER__

#include <iostream>
#include <string>
using namespace std;

class Developer {

private:
  string code;
  char * name;
  string email;

public:
  string get_code()const;
  void set_code(const string s);
  char * get_name()const;
  void set_name(const char * s);
  string get_email()const;
  void set_email(const string s);

  Developer();
  ~Developer();
  Developer(const Developer &d);
  Developer(const string c, const string n, const string e);
  Developer& operator=(const Developer &d);
  bool operator==(const Developer &d);
  bool operator<(const Developer &d);
  friend ostream & operator<<(ostream &o, const Developer &d);

};

#endif
