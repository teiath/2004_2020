#include <string.h>		// strlen(), strncpy()
#include "developer.h"
#include "common.h"

string Developer::get_code()const {
  return code;
}
void Developer::set_code(const string s) {
  code = s;
};


char * Developer::get_name()const {
  return name;
}
void Developer::set_name(const char * s) {
  if (name) delete[] name;
  char *p = new char[strlen(s)+1];
  strncpy(p, s, strlen(s));
  p[strlen(s)] = (char) 0;
  name = p;
};

string Developer::get_email()const {
  return email;
}
void Developer::set_email(const string s) {
  email = s;
};

Developer::Developer() {
  code = "";
  char *p = new char[1];
  *p = (char) 0;
  name = p;
  email = "";
}

Developer::Developer(const Developer &d){
  code = d.code;
  char *p = new char[strlen(d.name)+1];
  strncpy(p, d.name, strlen(d.name));
  p[strlen(d.name)] = (char)0;
  name = p;
  email = d.email;
}

Developer::Developer(const string c, const string n, const string e) {
  code = c;
  char *p = new char[n.size()+1];
  strncpy(p, n.c_str(), n.size());
  p[n.size()] = (char)0;
  name = p;
  email = e;
}

Developer::~Developer() {
  if (name) delete [] name;
}

Developer & Developer::operator=(const Developer &d) {
  if (this == &d) return *this;

  code = d.code;
  if (name) delete[] name;
  char *p = new char[strlen(d.name)+1];
  strncpy(p, d.name, strlen(d.name));
  p[strlen(d.name)] = (char)0;
  name = p;
  email = d.email;
  return *this;
}

bool Developer::operator==(const Developer &d){
  //σύγκριση βάσει κωδικών
 return (d.code == code);
}

bool Developer::operator<(const Developer &d){
  //σύγκριση βάσει κωδικών
 return (code < d.code);
}

ostream & operator<<(ostream &o, const Developer &d) {
  o << d.code << CSV_delimiter << d.name << CSV_delimiter << d.email;
}


#ifdef TEST_UNIT
int main () {


  cout  << "Δοκιμή αρχικοποίησης του κατασκευαστή d1:" << endl;
  Developer d1("DEV1", "όνομα_1", "dev1@mail.com");
  cout << "d1: " <<  d1 << endl;
  cout << endl;

  cout  << "Δοκιμή αρχικοποίησης του κατασκευαστή d2 με αντιγραφή του d1)" << endl;
  Developer d2(d1);
  cout << "d2: " <<  d2 << endl;
  cout << endl;

  cout  << "Δοκιμή εκχώρησης του d1 στον d2:" << endl; 
  d2 = d1;
  cout << "d2: " << d2 << endl;
  cout << endl;

  cout  << "Δοκιμή σύγκρισης του d1 με τον d2:" << endl; 
  cout << "d1 == d2: " << (d1 == d2) << endl;
  cout << "d1  < d2: " << (d1  < d2) << endl;
  cout << endl;

  Developer d3("DEV3", "όνομα_1", "dev1@mail.com");
  cout << "d1: " << d1 << endl;
  cout << "d3: " << d3 << endl;
  cout  << "Δοκιμή σύγκρισης του d1 με τον d3:" << endl; 
  cout << "d1 == d3: " << (d1 == d3) << endl;
  cout << "d1  < d3: " << (d1  < d3) << endl;
  cout << endl;

}


#endif				//TEST_UNIT
