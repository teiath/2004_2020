#include <set>
#include "developerSet.h"

// Συναρτήσεις == και < για την υλοποίηση συγκρίσεων στο παραπάνω set
bool operator==(const Developer& r1, const Developer& r2) {
  // σύγκριση βάσει κωδικού
  return (r1.get_code() == r2.get_code());
}

bool operator<(const Developer& r1, const Developer& r2) {
  // ταξινόμηση βάσει κωδικού
  return (r1.get_code() < r2.get_code());
}


// Προσθήκη στην δομή δεδομένων της μνήμης ενός αντικειμένου τύπου Developer
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool DeveloperSet_add(set<Developer> &S, const Developer &r) {
  pair<std::set<Developer>::iterator,bool> ret;
  Developer *tmp = new Developer;
  *tmp = r;
  ret = S.insert(*tmp);
  return ret.second;
};


// Διαγραφή του αποθηκευμένου στην δομή δεδομένων αντικειμένου τύπου Developer
// με κωδικό devcode
// Επιστρέφει ως bool την επιτυχία/αποτυχία
bool DeveloperSet_del(set<Developer> &S, const Developer &r) {
  auto i=S.begin();
  Developer *tmp = new Developer;
  *tmp = r;
  return (S.erase(*tmp) == 1);
};


using DeveloperFtype = bool(const Developer ss);
// εκτέλεσε την F(x) για όλα τα x στο S
// ή μέχρι η F να επιστρέψει false
void applyDeveloper(set<Developer> &S, DeveloperFtype & F) {
  auto i=S.begin();
  Developer tmp;
  while(i != S.end()) {
    tmp = *i;
    if (! F(tmp)) break;
    i++;
  }
}






#ifdef TEST_UNIT
#include <iostream>
#include <iomanip>		// setprecision()

bool show_set_member(const Developer d) {
  cout  << d << endl;
  return true;
}

int main () {
set<Developer> SD;

  Developer d1("DEV1", "όνομα_1", "dev1@mail.com");
  cout << "d1: " <<  d1 << endl;
  cout << "Δοκιμή προσθήκης d1 στο σύνολο: ";
  cout << (DeveloperSet_add(SD, d1) ? "OK" : "απέτυχε") << endl;
  cout << endl;

  Developer d2 = d1;
  cout << "d2: " << d2 << endl;
  cout << "Δοκιμή προσθήκης d2 στο σύνολο: ";
  cout << (DeveloperSet_add(SD, d2) ? "OK" : "απέτυχε") << endl;
  cout << "(δεν επιτρέπεται η αποθήκευση κατασκευαστή με τον ίδιο κωδικό)" << endl;
  cout << endl;

  Developer d3 = d1;
  d3.set_code("DEV3");
  d3.set_name("Κατασκευαστής_λογισμικού_3");
  d3.set_email("me@email3.com");
  cout << "d3: " <<  d3 << endl;
  cout << "Δοκιμή προσθήκης d3 στο σύνολο: ";
  cout << (DeveloperSet_add(SD, d3) ? "OK" : "απέτυχε") << endl;
  cout << endl;

  Developer d4 = d1;
  d4.set_code("DEV4");
  d4.set_name("Κατασκευαστής_λογισμικού_4");
  d4.set_email("me@email4.com");
  cout << "d4: " <<  d4 << endl;
  cout << "Δοκιμή προσθήκης d4 στο σύνολο: ";
  cout << (DeveloperSet_add(SD, d4) ? "OK" : "απέτυχε") << endl;
  cout << endl;


  cout << "Εκτύπωση όλων των αποθηκευμένων στοιχείων:" << endl;
  applyDeveloper(SD, show_set_member);
  cout << endl;

  cout << "Δοκιμή διαγραφής του κατασκευαστή DEV1 από το σύνολο: ";
  cout << (DeveloperSet_del(SD, d1) ? "OK" : "απέτυχε") << endl;
  applyDeveloper(SD, show_set_member);
  cout << endl;

  cout << "Δοκιμή διαγραφής ξανά του κατασκευαστή DEV1 από το σύνολο: ";
  cout << (DeveloperSet_del(SD, d1) ? "OK" : "απέτυχε") << endl;
  cout << "(δεν μπορεί να διαγραφεί ένας κατασκευαστής που δεν υπάρχει)" << endl;
  applyDeveloper(SD, show_set_member);
  cout << endl;

  Developer d5("DEV3", "όνομα_5", "hi@mail5.com");
  cout << "d5: " <<  d5 << endl;
  cout << "Δοκιμή εισαγωγής κατασκευαστή d5 με τον ίδιο κωδικό με άλλον: ";
  cout << (DeveloperSet_add(SD, d5) ? "OK" : "απέτυχε") << endl;
  applyDeveloper(SD, show_set_member);
  cout << endl;

}


#endif				//TEST_UNIT
