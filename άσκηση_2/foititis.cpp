#include "iostream"
#include <iomanip>		// setprecision()
#include "string"
#include "string.h"		// strlen(), strncpy()
#include "foititis.h"
using namespace std;

char *foititis::new_AM(const char *a) {
  char *t = new char[strlen(a) + 1];
  strncpy(t, a, strlen(a));
  t[strlen(a)] = '\0';
  return t;
};

float *foititis::new_vathmoi(float *v, unsigned int p) {
  if (v == NULL) {
    return NULL;
  }
  float *q = new float[p];
  for (int i = 0; i < p; i++)
    q[i] = v[i];
  return q;
};


float foititis::mesos_oros(void) {
  if (perasmena == 0) {
    return 0;
  } else {
    float sum = 0;
    for (int i=0; i<perasmena; i++) {
      sum = sum + vathmoi[i];
    }
    return sum / perasmena;
  }
}


char *foititis::get_AM(void) {
  return AM;
};

string foititis::get_onoma(void) {
  return onoma;
};

unsigned int foititis::get_examino(void) {
  return examino;
};

unsigned int foititis::get_perasmena(void) {
  return perasmena;
};

float *foititis::get_vathmoi(void) {
  return vathmoi;
};

void foititis::set_AM(const char *a) {
  if (AM) delete[] AM;
  AM = new_AM(a);
};

void foititis::set_onoma(const string o) {
  onoma = o;
};

void foititis::set_examino(const unsigned int e) {
  examino = e;
};

void foititis::set_perasmena(const unsigned int p) {
  if (p == 0) {
    perasmena = 0;
    // η δεσμευμένη μνήμη για τους παλαιούς βαθμούς δεν χρειάζεται πλέον
    if (vathmoi) delete[] vathmoi;
    vathmoi = (float *) NULL;
    return;
  }
  perasmena = p;
};


void foititis::set_vathmoi(float *v) {
  // η δεσμευμένη μνήμη για τους παλαιούς βαθμούς δεν χρειάζεται πλέον
  if (vathmoi) delete[] vathmoi;

  if (v == NULL) {
    perasmena = 0;
    vathmoi = (float *) NULL;
  } else {
    vathmoi = new_vathmoi(v, perasmena);
  }

};



foititis::foititis(const char *a, string o) {
  AM = new_AM(a);
  onoma = o;
  examino = 1;
  perasmena = 0;
  vathmoi = (float *)NULL;
};

foititis::foititis(const char *a, string o, const unsigned int e) {
  AM = new_AM(a);
  onoma = o;
  examino = e;
  perasmena = 0;
  vathmoi = (float *)NULL;
};

foititis::foititis(const char *a, string o, const unsigned int e,
                   const unsigned int p, float *v) {
  AM = new_AM(a);
  onoma = o;
  examino = e;
  perasmena = p;
  vathmoi = new_vathmoi(v, p);
};

foititis::foititis(foititis const &f) {
  AM = new_AM(f.AM);
  onoma = f.onoma;
  examino = f.examino;
  perasmena = f.perasmena;
  vathmoi = new_vathmoi(f.vathmoi, f.perasmena);
};

foititis::~foititis() {
  if (AM != NULL)
    delete[]AM;
  if (vathmoi != NULL)
    delete[]vathmoi;
};

foititis &
foititis::operator=(const foititis &f) {
  if (this == &f) {
    return *this;
  }
  set_AM(f.AM);
  set_onoma(f.onoma);
  set_examino(f.examino);
  set_perasmena(f.perasmena);
  set_vathmoi(f.vathmoi);
  return *this;
}

void
foititis::neos_vathmos(const float f) {
  float *q = new float[perasmena+1];
  for (int i = 0; i < perasmena; i++)
    q[i] = vathmoi[i];
  q[perasmena] = f;
  vathmoi = q;
  perasmena = perasmena + 1;
};



void foititis::ektypwsh3(ostream & c) {
  c << "Α/Μ: " << AM << "\t" << onoma << "  [ εξάμηνο:" << examino
    << ", μαθήματα:" << perasmena << " ]" << "\n";
//<< "  Ονοματεπώνυμο: " << onoma << "\n";
}

void foititis::ektypwsh(ostream & c) {
  // οι πραγματικοί να τυπώνονται με 2 δεκαδικά
  c <<fixed << setprecision(2);
  foititis::ektypwsh3(c);
  for (int i=0; i<perasmena; i++) {
    c << "\t" << "ΜΑΘΗΜΑ_"  << (i+1) << "\t" << vathmoi[i] << "\n";
  }
  c  << "\t\t--- Μέσος όρος βαθμολογίας: "<< mesos_oros() << "\n";
};

