#include "iostream"
#include "string"
using namespace std;


class foititis {
private:
  char *AM;
  string onoma;
  unsigned int examino;
  unsigned int perasmena;
  float *vathmoi;

public:
  char *get_AM(void);
  string get_onoma(void);
  unsigned int get_examino(void);
  unsigned int get_perasmena(void);
  float *get_vathmoi(void);
  void set_AM(const char *a);
  void set_onoma(const string o);
  void set_examino(const unsigned int e);
  void set_perasmena(const unsigned int p);
  void set_vathmoi(float *v);

  foititis(const char *a, const string o);
  foititis(const char *a, string o, const unsigned int e);
  foititis(const char *a, string o, const unsigned int e,
           const unsigned int p, float *v);
  foititis(foititis const &f);
  ~foititis();
  foititis &operator=(const foititis &f);
  void neos_vathmos(const float f);
  void ektypwsh3(ostream & c);
  void ektypwsh(ostream & c);

private:
  char *new_AM(const char *a);
  float *new_vathmoi(float *v, unsigned int p);
  float mesos_oros(void);
};
