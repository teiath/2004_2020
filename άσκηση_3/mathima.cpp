#include "mathima.h"

using namespace std;

void mathima::set_kwdikos(const string k) {
  kwdikos = k;
}

void mathima::set_onoma(const string o) {
  onoma = o;
}

void mathima::set_examino(const unsigned int e) {
  examino = e;
}

string mathima::get_kwdikos()const {
  return kwdikos;
}

string mathima::get_onoma()const {
  return onoma;
}

unsigned int mathima::get_examino()const {
  return examino;
}

mathima::mathima() {
  kwdikos = "";
  onoma = "";
  examino = 0;
}


mathima::mathima(string k, string o, unsigned int e) {
  kwdikos = k;
  onoma = o;
  examino = e;
}

mathima &
mathima::operator=(const mathima &m) {
  if (this == &m) {
    return *this;
  }
  set_kwdikos(m.kwdikos);
  set_onoma(m.onoma);
  set_examino(m.examino);
  return *this;
}

ostream &
operator<<(ostream &o, const mathima &m) {
  o << "Εξαμ:" << m.get_examino()
    << "\t" << m.get_kwdikos()
    << "\t" << m.get_onoma();
  return o;
}
