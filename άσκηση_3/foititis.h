#include "iostream"
#include "string"
#include "mathima.h"

using namespace std;


class foititis {
private:
  char *AM;
  string onoma;
  unsigned int examino;

  unsigned int nperasmena;	// πλήθος περασμένων μαθημάτων
  float *perasmena;		// βαθμοί περασμένων μαθημάτων

  unsigned int ndilomena;	// πλήθος δηλωμένων μαθημάτων
  mathima *dilomena;		// δηλωμένα μαθήματα

public:
  char *get_AM(void);
  string get_onoma(void);
  unsigned int get_examino(void);
  unsigned int get_nperasmena(void);
  float* get_perasmena(void);
  unsigned int get_ndilomena();
  mathima* get_dilomena();

  void set_AM(const char *a);
  void set_onoma(const string o);
  void set_examino(const unsigned int e);
  void set_nperasmena(const unsigned int p);
  void set_perasmena(float *v);
  void set_ndilomena(const unsigned int n);
  void set_dilomena(mathima *m);

  foititis(const char *a, const string o);
  foititis(const char *a, string o, const unsigned int e);
  foititis(const char *a, string o, const unsigned int e,
           const unsigned int p, float *v);
  foititis(foititis const &f);
  ~foititis();
  foititis &operator=(const foititis &f);
  void neos_vathmos(const float f);
  void ektypwsh3(ostream & c);
  void ektypwsh(ostream & c);

  foititis &operator+=(const mathima& m);

  bool operator==(const foititis &m);
  bool operator!=(const foititis &m);
  bool operator<=(const foititis &m);
  bool operator>=(const foititis &m);
  bool operator<(const foititis &m);
  bool operator>(const foititis &m);

private:
  char *new_AM(const char *a);
  float *new_perasmena(float *p, unsigned int n);
  float mesos_oros(void);
  mathima *new_dilomena(mathima *m, unsigned int n);
};
