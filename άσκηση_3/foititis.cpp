#include <iomanip>	// setprecision()
#include "string.h"	// strlen(), strncpy()

#include "iostream"
#include "string"
#include "foititis.h"
using namespace std;

char *foititis::new_AM(const char *a) {
  char *t = new char[strlen(a) + 1];
  strncpy(t, a, strlen(a));
  t[strlen(a)] = '\0';
  return t;
};

float *foititis::new_perasmena(float *p, unsigned int n) {
  if (p == NULL) {
    return NULL;
  }
  float *q = new float[n];
  for (int i = 0; i < n; i++)
    q[i] = p[i];
  return q;
};


float foititis::mesos_oros(void) {
  if (nperasmena == 0) {
    return 0;
  } else {
    float sum = 0;
    for (int i=0; i<nperasmena; i++) {
      sum = sum + perasmena[i];
    }
    return sum / nperasmena;
  }
}


char *foititis::get_AM(void) {
  return AM;
};

string foititis::get_onoma(void) {
  return onoma;
};

unsigned int foititis::get_examino(void) {
  return examino;
};

unsigned int foititis::get_nperasmena(void) {
  return nperasmena;
};

float *foititis::get_perasmena(void) {
  return perasmena;
};

unsigned int foititis::get_ndilomena(void) {
  return ndilomena;
};

mathima *foititis::get_dilomena(void) {
  return dilomena;
};

void foititis::set_AM(const char *a) {
  if (AM) delete[] AM;
  AM = new_AM(a);
};

void foititis::set_onoma(const string o) {
  onoma = o;
};

void foititis::set_examino(const unsigned int e) {
  examino = e;
};

void foititis::set_nperasmena(const unsigned int p) {
  if (p == 0) {
    nperasmena = 0;
    // η δεσμευμένη μνήμη για τους παλαιούς βαθμούς δεν χρειάζεται πλέον
    if (perasmena) delete[] perasmena;
    perasmena = (float *) NULL;
    return;
  }
  nperasmena = p;
};


void foititis::set_perasmena(float *v) {
  // η δεσμευμένη μνήμη για τους παλαιούς βαθμούς δεν χρειάζεται πλέον
  if (perasmena) delete[] perasmena;

  if (v == NULL) {
    nperasmena = 0;
    perasmena = (float *) NULL;
  } else {
    perasmena = new_perasmena(v, nperasmena);
  }

};


mathima *foititis::new_dilomena(mathima *m, unsigned int n) {
  if (m == NULL) {
    return NULL;
  }
  mathima *q = new mathima[n];
  for (int i = 0; i < n; i++)
    q[i] = m[i];
  return q;
};


void foititis::set_ndilomena(const unsigned int p) {

  if (p == 0) {
    ndilomena = 0;
    // η δεσμευμένη μνήμη για τους παλαιά μαθήματα δεν χρειάζεται πλέον
    if (dilomena) delete[] dilomena;
    dilomena = (mathima *) NULL;
    return;
  }
  ndilomena = p;
};


void foititis::set_dilomena(mathima *m) {
  // η δεσμευμένη μνήμη για τους παλαιούς βαθμούς δεν χρειάζεται πλέον
  if (dilomena) delete[] dilomena;

  if (m == NULL) {
    ndilomena = 0;
    dilomena = (mathima *) NULL;
  } else {
    dilomena = new_dilomena(m, ndilomena);
  }

};



foititis::foititis(const char *a, string o) {
  AM = new_AM(a);
  onoma = o;
  examino = 1;
  nperasmena = 0;
  perasmena = (float *)NULL;
  ndilomena = 0;
  dilomena = (mathima *)NULL;
};

foititis::foititis(const char *a, string o, const unsigned int e) {
  AM = new_AM(a);
  onoma = o;
  examino = e;
  nperasmena = 0;
  perasmena = (float *)NULL;
  ndilomena = 0;
  dilomena = (mathima *)NULL;
};

foititis::foititis(const char *a, string o, const unsigned int e,
                   const unsigned int p, float *v) {
  AM = new_AM(a);
  onoma = o;
  examino = e;
  nperasmena = p;
  perasmena = new_perasmena(v, p);
  ndilomena = 0;
  dilomena = (mathima *)NULL;
};

foititis::foititis(foititis const &f) {
  AM = new_AM(f.AM);
  onoma = f.onoma;
  examino = f.examino;
  nperasmena = f.nperasmena;
  perasmena = new_perasmena(f.perasmena, f.nperasmena);
  ndilomena = 0;
  dilomena = (mathima *)NULL;
};

foititis::~foititis() {
  if (AM != NULL)
    delete[]AM;
  if (perasmena != NULL)
    delete[]perasmena;
};

foititis &
foititis::operator=(const foititis &f) {
  if (this == &f) {
    return *this;
  }
  set_AM(f.AM);
  set_onoma(f.onoma);
  set_examino(f.examino);
  set_nperasmena(f.nperasmena);
  set_perasmena(f.perasmena);
  return *this;
}

foititis &
foititis::operator+=(const mathima& m) {
  mathima *nm = new mathima;
  *nm = m;

  mathima *nmp = new mathima[ndilomena + 1];
  for (unsigned int  i=0; i<ndilomena; i++) {
    nmp[i] = dilomena[i];
  }
  nmp[ndilomena] = *nm;
  dilomena = new_dilomena(nmp, ndilomena + 1);
  ndilomena = ndilomena+1;
  return *this;
}

void
foititis::neos_vathmos(const float f) {
  float *q = new float[nperasmena+1];
  for (int i = 0; i < nperasmena; i++)
    q[i] = perasmena[i];
  q[nperasmena] = f;
  perasmena = q;
  nperasmena = nperasmena + 1;
};



void foititis::ektypwsh3(ostream & c) {
  c << "Α/Μ: " << AM << "\t" << onoma << "  [ εξάμηνο:" << examino
    << ", μαθήματα:" << nperasmena << " ]" << "\n";
//<< "  Ονοματεπώνυμο: " << onoma << "\n";
}

void foititis::ektypwsh(ostream & c) {
  // οι πραγματικοί να τυπώνονται με 2 δεκαδικά
  c <<fixed << setprecision(2);
  foititis::ektypwsh3(c);

  for (unsigned int i=0; i<ndilomena; i++) {
    if (i==0) {
      c << "  Δηλωμένα μαθήματα:\n";
    }
    c << "\t" << dilomena[i] << "\n";
  }

  for (unsigned int i=0; i<nperasmena; i++) {
    if (i==0) {
      c << "  Περασμένα μαθήματα:\n";
    }
    c << "\t" << "ΜΑΘΗΜΑ_"  << (i+1) << "\t" << perasmena[i] << "\n";
  }
  c  << "\t\t--- Μέσος όρος βαθμολογίας: "<< mesos_oros() << "\n";
};

bool foititis::operator==(const foititis &m) {
  return (examino == m.examino);
}
bool foititis::operator<(const foititis &m) {
  return (examino < m.examino);
}
bool foititis::operator!=(const foititis &m) {
  return (!(examino == m.examino));
}
bool foititis::operator<=(const foititis &m) {
  return ((examino == m.examino)||(examino < m.examino));
}
bool foititis::operator>(const foititis &m) {
  return (!(examino <= m.examino));
}
bool foititis::operator>=(const foititis &m) {
  return (!(examino < m.examino));
}
