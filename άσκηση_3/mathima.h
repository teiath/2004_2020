#include <string>
#include <iostream>
using namespace std;

class mathima {
private:
  string kwdikos;
  string onoma;
  unsigned int examino;

public:
  void set_kwdikos(const string k);
  void set_onoma(const string o);
  void set_examino(const unsigned int e);
  string get_kwdikos() const;
  string get_onoma() const;
  unsigned int get_examino() const;
  mathima();
  mathima(string k, string o, unsigned int e);

  mathima & operator=(const mathima &m);
  friend ostream & operator<<(ostream &o, const mathima &m);
};
