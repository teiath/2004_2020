// Διαχείριση αντικειμένων τύπου T σε δομές map

#include <map>
#include "mapOps.hpp"
using namespace std;

template <typename T>
bool map_insert(map<strng, T&> &m, strng s, T &t) {
  auto ret = m.insert(pair <strng,T&> (s, t));
  return ret.second;
}

template <typename T>
bool map_delete(map<strng, T&> &m, const strng s) {
  auto ret = m.erase(s);
  return (ret == 1);
}

template <typename T>
bool map_exists(map<strng, T&> &m, const strng s) {
  auto ret = m.find(s);
  return (ret != m.end());
}



#ifdef TEST_UNIT
#include <iostream>
#include "student.hpp"
#define	DATATYPE	Student

void showall(ostream &o, map<strng, DATATYPE&> &s) {
  for (auto it=s.begin(); it!=s.end(); ++it)
    o << it->first << " => " 
      << (it->second)
      << '\n';
}
main() {
  map<strng, DATATYPE &> S;

  DATATYPE
    A("ΚΑΛΛΙΟΠΗ", "ΗΛΙΑΔΗ"),
    B("ΜΗΝΑΣ", "ΗΛΙΑΔΗΣ"),
    C("ΗΛΙΑΣ", "ΠΑΠΑΔΟΠΟΥΛΟΣ"),
    D("ΜΑΡΙΟΣ", "ΚΩΒΑΙΟΣ"),
    E("ΖΗΣΗΣ", "ΚΩΒΑΙΟΣ"),
    F("ΖΑΧΑΡΙΑΣ", "ΚΩΒΑΙΟΣ");
  DATATYPE G = B, H(A);

    cout << "Εισαγωγή αντικειμένων:\n";
				// Εισαγωγή με τυχαία σειρά
    cout << "D: "<<D<<" "<<(map_insert(S, "CODE_D", D)?"OK":"απέτυχε") << endl;
    cout << "C: "<<C<<" "<<(map_insert(S, "CODE_C", C)?"OK":"απέτυχε") << endl;
    cout << "A: "<<A<<" "<<(map_insert(S, "CODE_A", A)?"OK":"απέτυχε") << endl;
    cout << "H: "<<H<<" "<<(map_insert(S, "CODE_H", H)?"OK":"απέτυχε") << endl;
    cout << "F: "<<F<<" "<<(map_insert(S, "CODE_F", F)?"OK":"απέτυχε") << endl;
    cout << "E: "<<E<<" "<<(map_insert(S, "CODE_E", E)?"OK":"απέτυχε") << endl;
    cout << "B: "<<B<<" "<<(map_insert(S, "CODE_B", B)?"OK":"απέτυχε") << endl;
    cout << "G: "<<G<<" "<<(map_insert(S, "CODE_G", G)?"OK":"απέτυχε") << endl;
    cout << "G: "<<G<<" "<<(map_insert(S, "CODE_G", G)?"OK":"απέτυχε") << endl;
    cout << "(προσθήκη αντικειμένου με ίδιο κωδικό δεν επιτρέπεται)" << endl;
    cout << endl;
    cout << "Εμφάνιση αντικειμένων:\n";
    showall(cout, S);
    cout << endl;

    cout << "Διαγραφή αντικειμένων:\n";
				// Εισαγωγή με τυχαία σειρά
    cout << "CODE_D: "<<D<<" "<<(map_delete(S, "CODE_D")?"OK":"απέτυχε") << endl;
    cout << "CODE_C: "<<C<<" "<<(map_delete(S, "CODE_C")?"OK":"απέτυχε") << endl;
    cout << "CODE_C: "<<C<<" "<<(map_delete(S, "CODE_C")?"OK":"απέτυχε") << endl;
    cout << "(διαγραφή αντικειμένου που δεν υπάρχει πλέον δεν επιτρέπεται)" << endl;
				// Εμφάνιση ταξινομημένα ως προς το κλειδί
    cout << endl;

    cout << "Εμφάνιση αντικειμένων:\n";
    showall(cout, S);
    cout << endl;

    cout << "Εύρεση αντικειμένων:\n";
    cout << "CODE_B: "<<B<<" "<<(map_exists(S, "CODE_B")?"OK":"απέτυχε") << endl;
    cout << "CODE_C: "<<C<<" "<<(map_exists(S, "CODE_C")?"OK":"απέτυχε") << endl;

}
#endif		// TEST_UNIT
