#include <stdio.h>		// snprintf()
using namespace std;

#include "strng.hpp"

const unsigned int strng::size() const {
  return length;
}

const char *strng::c_str() const {
  if (length < 0) {		// negative length?
    return (char *)0;		// returns NULL
  }

  if (length >= 0) {
    char *p = new char[length+1];
    char *from=data, *to=p;
    unsigned int s = 0;
    while (s < length) {
      *to = *from;
      s++;
      from++;
      to++;
    }
    *to = (char)0;		// end of C string
    return p;
  }
}

strng::operator bool() const {
  return (length > 0);
}

void strng::set(const char* cp){
  size_t alength=0;
  const char *t = cp;
  while (*t) {
    alength++;  // αντί για strlen(*cp)
    t++;
  }

  if (alength == 0) {
    length = 0;
    data = NULL;
  } else {
    char *n = new char[alength];
    data = n;
    t = cp;
    while (*t) {
      *n=*t;  //  αντί για  memcpy(n, *cp, alength);
      n++;
      t++;
    }
  }
  length = alength;
}


strng::strng() {
  length = 0;
  data = NULL;
}

strng::~strng() {
  if (data != NULL) {
    delete [] data;
  }
}

strng::strng(const char c) {
  length = 1;
  char *n = new char[length];
  *n = c;
  data = n;
}

strng::strng(const char *a) {
  size_t alength=0;
  const char *cp = a;

  while (*cp) {
    alength++;  // αντί για strlen(a)
    cp++;
  }

  if (alength == 0) {
    length = 0;
    data = NULL;
  } else {
    char *n = new char[alength];
    data = n;
    cp = a;
    while (*cp) {
      *n=*cp;  //  αντί για  memcpy(n, a, alength);
      n++;
      cp++;
    }
  }
  length = alength;
};

strng & strng::operator=(const strng & s) {
  if (this == &s)
    return *this;

  if (data != NULL) delete[] data;

  if (s.length == 0) {
    length = 0;
    data = NULL;
    return *this;
  }

  char *p, *n;
  n = new char[s.length];
  length = s.length;
  data = n;
  p = s.data;
  for (unsigned int i = 0; i < s.length; i++) {
    *n = *p;
    p++;
    n++;
  }
  return *this;
};

strng::strng(const strng & s) {

  if (s.length == 0) {
    length = 0;
    data = NULL;
  } else {
    char *p, *n;
    n = new char[s.length];
    length = s.length;
    data = n;
    p = s.data;
    for (unsigned int i = 0; i < s.length; i++) {
      *n = *p;
      p++;
      n++;
    }
  }
};

bool strng::operator==(const strng & s) const {
  if (s.length != (*this).length)
    return false;
  for (unsigned int i = 0; i < s.length; i++) {
    if (*(s.data + i) != *((this->data) + i))
      return false;
  }
  return true;
}

bool strng::operator!=(const strng & s) const {
  return (!(*this == s));
};

bool strng::operator<(const strng & s) const {
  unsigned int minlength = length;
  if (s.length < minlength) minlength = s.length;

  char *td = data, *sd = s.data;
  for (unsigned int i=0; i<minlength; i++) {
    if ((*td) > (*sd)) {
      return false;
    }
    if ((*td) < (*sd)) {
      return true;
    }
    td++;
    sd++;
  }

  // Οι πρώτοι minlength χαρακτήρες στα δύο strng είναι ίδιοι,
  // άρα ας συγκρίνουμε απλώς τα μήκη τους
  if (length < s.length) return true;	// μικροτέρου μήκους
  return false;
}

bool strng::operator<=(const strng & s) const {
  return ( (*this == s) || (*this < s) );
}

bool strng::operator>(const strng & s) const {
  return (!(*this <= s));
}
bool strng::operator>=(const strng & s) const {
  return (s <= *this);
}

std::ostream & operator<<(std::ostream & o, const strng & s) {
  if (s) {
    o << s.c_str();
  }
  return o;
};

strng & strng::operator+=(const strng & s) {
  if (s.length == 0) {	// δεν χρειάζεται να προσθέσουμε κάτι
    return *this;
  }

  char *p, *n, *d;
  d = new char[s.length+length];

  p = data;		// αντιγραφή του εαυτού μας
  n = d;
  for (unsigned int i = 0; i < length; i++) {
    *n = *p;
    p++;
    n++;
  }
  if (data != NULL) delete[] data;

  p = s.data;		// αντιγραφή του ορίσματος
  for (unsigned int i = 0; i < s.length; i++) {
    *n = *p;
    p++;
    n++;
  }


  length += s.length;
  data = d;
  return *this;
};


strng::strng(const long long int l) {
  char num[256], *nump = &num[0];
  snprintf(nump, sizeof(num)/sizeof (num[0]), "%lld", l);
  size_t alength=0;
  const char *cp = nump;

  while (*cp) {
    alength++;  // αντί για strlen(nump)
    cp++;
  }

  if (alength == 0) {
    length = 0;
    data = NULL;
  } else {
    char *n = new char[alength];
    data = n;
    cp = nump;
    while (*cp) {
      *n=*cp;  //  αντί για  memcpy(n, nump, alength);
      n++;
      cp++;
    }
  }
  length = alength;
};

      

strng::strng(const int i) {
  char num[256], *nump = &num[0];
  snprintf(nump, sizeof(num)/sizeof (num[0]), "%d", i);
  size_t alength=0;
  const char *cp = nump;

  while (*cp) {
    alength++;  // αντί για strlen(nump)
    cp++;
  }

  if (alength == 0) {
    length = 0;
    data = NULL;
  } else {
    char *n = new char[alength];
    data = n;
    cp = nump;
    while (*cp) {
      *n=*cp;  //  αντί για  memcpy(n, nump, alength);
      n++;
      cp++;
    }
  }
  length = alength;
};




strng::strng(const float f) {
  char num[256], *nump = &num[0];
  snprintf(nump, sizeof(num)/sizeof (num[0]), "%f", f);
  size_t alength=0;
  const char *cp = nump;

  while (*cp) {
    alength++;  // αντί για strlen(nump)
    cp++;
  }

  if (alength == 0) {
    length = 0;
    data = NULL;
  } else {
    char *n = new char[alength];
    data = n;
    cp = nump;
    while (*cp) {
      *n=*cp;  //  αντί για  memcpy(n, nump, alength);
      n++;
      cp++;
    }
  }
  length = alength;
};






#ifdef TEST_UNIT
int main() {

  strng A(""), B("xyz"), C, D, E(B), F;
  cout << "A: <" << A.c_str() << ">  <" << A << ">\tsize: " << A.
       size() << "\n";
  cout << "(bool)A? " << (bool)A << "\n";

  cout << "B: <" << B.c_str() << ">  <" << B << ">\tsize: " << B.
       size() << "\n";
  B = B;
  cout << "(bool)B? " << (bool)B << "\n";
  cout << "B: <" << B.c_str() << ">  <" << B << ">\tsize: " << B.
       size() << "\n";
  cout << "C: <" << C.c_str() << ">  <" << C << ">\tsize: " << C.
       size() << "\n";
  D = B;
  cout << "B: <" << B.c_str() << ">  <" << B << ">\tsize: " << B.
       size() << "\n";
  cout << "D: <" << D.c_str() << ">  <" << D << ">\tsize: " << D.
       size() << "\n";
  cout << "B == D? " << (B == D) << "\n";

  cout << "E: <" << E.c_str() << ">\tsize: " << E.size() << "\n";
  E.set("AB CD EF");
  cout << "E: <" << E.c_str() << ">\tsize: " << E.size() << "\n";
  E = "xyw";
  cout << "E: <" << E.c_str() << ">\tsize: " << E.size() << "\n";
  cout << "B == E? " << (B == E) << "\tB != E? " << (B != E) << "\n";
  cout << "C == E? " << (C == E) << "\tC != E? " << (C != E) << "\n";
  cout << "E == E? " << (E == E) << "\tE != E? " << (E != E) << "\n";
  D = "ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ I";
  cout << "D: <" << D.c_str() << ">\tsize: " << D.size() << "\n";

  F = "ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ V";
  cout << "F: <" << F.c_str() << ">\tsize: " << F.size() << "\n";
  const strng G("ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ IV");
  cout << "G: <" << G.c_str() << ">  <" << G << ">\tsize: " << G.
       size() << "\n";

  cout << "D >= D? " << (D >= D) << "\tD > D? " << (D > D) << "\n";
  cout << "D >= F? " << (D >= F) << "\tD > F? " << (D > F) << "\n";
  cout << "D <= F? " << (D <= F) << "\tD < F? " << (D < F) << "\n";
  cout << "D >= G? " << (D >= G) << "\tD > G? " << (D > G) << "\n";
  cout << "D <= G? " << (D <= G) << "\tD < G? " << (D < G) << "\n";


  strng *Z = new strng;
  cout << "*Z: <" << Z->c_str()<< ">  <"<< *Z << ">\tsize: " << Z->size() << "\n";
  *Z = "ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ III";
  cout << "*Z: <" << Z->c_str()<< ">  <"<< *Z << ">\tsize: " << Z->size() << "\n";
  *Z = F;
  cout << "*Z: <" << Z->c_str()<< ">  <"<< *Z << ">\tsize: " << Z->size() << "\n";
  cout << "F: <" << F.c_str() << ">  <" << F << ">\tsize: " << F.  size() << "\n";
  F = "ΧΗΜΕΙΑ ΙΙ";
  cout << "F: <" << F.c_str() << ">  <" << F << ">\tsize: " << F.  size() << "\n";
  F = "ΑΝΟΡΓΑΝΗ ΧΗΜΕΙΑ ΙΙ";
  cout << "F: <" << F.c_str() << ">  <" << F << ">\tsize: " << F.  size() << "\n";

  E="ICE1-1001";
  F="ICE1-1005";
  cout << "E: "<<E << "\tF: " << F<< endl;
  cout << "E <= F? " << (E <= F) << "\tE < F? " << (E < F) << "\n";

  auto N = new strng;
  *N = "ΚΟΣΜΟΣ"; 
  cout << "N: " << *N << endl;
  delete N;

  int i = 27; long long int l = 123456789012345;
  strng s1(i), s2(l);
  cout << "strng(int): " << s1 << " " << (strng) i << endl;
  cout << "strng(long long int): " << s2 << " " << (strng)l << endl;
}

#endif				//TEST_UNIT
