// Αποθήκευση δεδομένων σε αρχεία CSV

#ifndef __WRITEFILE__
#define __WRITEFILE__

#include <iostream>
#include <fstream>
#include "strng.hpp"
#include "exairesi.hpp"
#include "common.hpp"

void write_file(int action, strng text);

#endif
