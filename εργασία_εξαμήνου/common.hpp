// Μερικοί βολικοί ορισμοί

#ifndef __COMMON__
#define __COMMON__


// Οι τύποι αρχείων στο δίσκο που διαχειρίζεται η εφαρμογή
enum DATAFILES {
  DFstudent=0,	// φοιτητές
  DFcourse,	// μαθήματα
  DFequivalent,	// ισοδυναμίες μαθημάτων
  DFgrade,	// βαθμοί

  DFerror,	// σφάλματα
  DFwarning	// προειδοποιήσεις
};



// ονόματα αρχείων τύπου CSV προς ανάγνωση (αρχεία εισόδου)
#define INP_student	"input_data/student_inp.csv"
#define INP_course	"input_data/course_inp.csv"
#define INP_equivalent	"input_data/equivalent_inp.csv"
#define INP_grade	"input_data/grade_inp.csv"

// ονόματα αρχείων τύπου CSV προς εγγραφή (αρχεία εξόδου)
#define OUT_errors	"report_errors.csv"
#define OUT_warnings	"report_warnings.csv"


// διαχωριστής πεδίων σε αρχείο CSV
#define CSV_delimiter	';'		// char

#endif
