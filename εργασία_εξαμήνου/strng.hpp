#ifndef __STRNG__
#define __STRNG__
#include <iostream>

class strng {

private:
  unsigned int length;
  char *data;

public:
  const unsigned int size() const ;	// μήκος
  const char* c_str() const;		// μετατροπή σε C string
  void set(const char* cp);		// μετατροπή από C string
  strng();
  ~strng();
  strng(const char *a);
  strng(const char c);
  strng(const strng &s);
  strng(const int n);
  strng(const long long int n);
  strng(const float f);

  operator bool() const;	// επιστρέφει true, αν το strng δεν είναι κενό

  strng &operator=(const strng &s);
  bool operator==(const strng &s)const;
  bool operator!=(const strng &s)const;
  bool operator<(const strng &s)const;
  bool operator<=(const strng &s)const;
  bool operator>(const strng &s)const;
  bool operator>=(const strng &s)const;

  strng &operator+=(const strng &s);	 // προσθήκη κατ' επέκταση στο τέλος

  friend std::ostream & operator<<(std::ostream & o, const strng & s);
};

#endif
