.headers off
select "";
select "### 1.	Εκκαθάριση βαθμολογιών έγκυρων φοιτητών/μαθημάτων";
select "   	(κρατώντας μόνο τον μεγαλύτερο βαθμό σε κάθε μάθημα";
select "   	και μόνο για τους φοιτητές/τα μαθήματα που περιέχονται";
select "   	στους πίνακες student/course αντιστοίχως):";
drop table if exists final_grade;
create table final_grade as
select distinct grade.studentid as studentid,
                grade.courseid as courseid, max(grade) as grade
	from grade,student,course
	where (student.studentid==grade.studentid
	       and course.courseid==grade.courseid)
	group by grade.studentid,grade.courseid ;
drop index if exists final_grade_studentid;
drop index if exists final_grade_courseid;
drop index if exists final_grade_grade;
create index final_grade_studentid on "final_grade" (studentid);
create index final_grade_courseid on "final_grade" (courseid);
create index final_grade_grade on "final_grade" (grade);
select count(1) from final_grade;
-- 36732 distinct results




.headers off
select "";
select "### 2.	Ποιοι έχουν διαφορετικό βαθμό σε αντίστοιχα μαθήματα:";
drop table if exists different_grade;
create table different_grade as
select distinct
	G1.StudentId as studentid,
	equivalent.OldCourseId as oldcourseid,
	G1.Grade as oldgrade,
	equivalent.NewCourseId as newcourseid,
	G2.Grade as newgrade
from final_grade as G1, final_grade as G2, equivalent
where
	(oldgrade != newgrade) and
	(G1.StudentId = G2.StudentId) and
	(equivalent.OldCourseId = G1.courseid) and
	(equivalent.NewCourseId = G2.courseid)
;
select count(1) from different_grade;
-- 477 distinct results



.headers off
select "";
select "### 3.	Ποιοι δεν έχουν βαθμό σε αντίστοιχο νέο μάθημα:";
drop table if exists missing_grade;
create table missing_grade as
select distinct
	G1.StudentId as studentid,
	equivalent.OldCourseId as oldcourseid,
	G1.Grade as oldgrade,
	equivalent.NewCourseId as newcourseid,
	"-"
from final_grade as G1, equivalent
where
G1.courseid = oldcourseid and
not exists (
		select 1
		from final_grade as G2
		where (G1.StudentId = G2.StudentId) and
		      (G2.courseid = newcourseid)
           )
;
select count(1) from missing_grade;
-- 3975 distinct results

