.headers off
select
	S.studentid, S.lastname, S.firstname,
	M.oldcourseid, C1.name,
	M.newcourseid, C2.name,
	M.oldgrade
from student as S, course as C1, course as C2, missing_grade as M
where
	(C1.courseid=M.oldcourseid
	and
	C2.courseid=M.newcourseid and S.studentid=M.studentid)
;
