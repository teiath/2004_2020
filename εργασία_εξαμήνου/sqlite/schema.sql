PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS "course"
(
    CourseId NVARCHAR(20) PRIMARY KEY NOT NULL,
    Name NVARCHAR(120)
);
--INSERT INTO "course" VALUES('P001','ΑΝΑΛΥΣΗ I');
--INSERT INTO "course" VALUES('P002','ΑΝΑΛΥΣΗ II');
--INSERT INTO "course" VALUES('ICE1','ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ I');
--INSERT INTO "course" VALUES('ICE2','ΜΑΘΗΜΑΤΙΚΗ ΑΝΑΛΥΣΗ II');
--INSERT INTO "course" VALUES('P302','ΔΙΑΝΟΗΤΙΚΗ');


CREATE TABLE IF NOT EXISTS "equivalent"
(
    OldCourseId NVARCHAR(20) NOT NULL,
    NewCourseId NVARCHAR(20) NOT NULL,
    equivalentId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    FOREIGN KEY (OldCourseId) REFERENCES "course" (CourseId)
		ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (NewCourseId) REFERENCES "course" (CourseId)
		ON DELETE NO ACTION ON UPDATE NO ACTION
);
--INSERT INTO "equivalent" (OldCourseId,NewCourseId) VALUES('P002','ICE2');
--INSERT INTO "equivalent" (OldCourseId,NewCourseId) VALUES('P001','ICE1');


CREATE TABLE IF NOT EXISTS "student"
(
    StudentId NVARCHAR(20) PRIMARY KEY NOT NULL,
    LastName NVARCHAR(50)  NOT NULL,
    FirstName NVARCHAR(50)  NOT NULL
);
--INSERT INTO "student" VALUES('02XA','ΠΑΝΤΑΖΗ','ΜΑΡΙΚΑ');
--INSERT INTO "student" VALUES('3MK1','ΚΥΡΙΑΖΗ','ΑΝΝΕΤA');


CREATE TABLE IF NOT EXISTS "grade"
(
    GradeId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    StudentId NVARCHAR(20) NOT NULL,
    CourseId NVARCHAR(20) NOT NULL,
    Grade REAL NOT NULL,
    FOREIGN KEY (StudentId) REFERENCES "student" (StudentId)
		ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (CourseId) REFERENCES "course" (CourseId)
		ON DELETE NO ACTION ON UPDATE NO ACTION
);
--INSERT INTO "grade" (StudentId,CourseId,Grade) VALUES('02XA','ICE1',4.5);
--INSERT INTO "grade" (StudentId,CourseId,Grade) VALUES('02XA','P001',3.5);
--INSERT INTO "grade" (StudentId,CourseId,Grade) VALUES('02XA','P002',7.0);
--INSERT INTO "grade" (StudentId,CourseId,Grade) VALUES('02XA','ICE2',7.0);
--INSERT INTO "grade" (StudentId,CourseId,Grade) VALUES('3MK1','P002',8.0);

CREATE INDEX Idx_GradeCourseId ON "grade" (CourseId);
CREATE INDEX Idx_GradeStudentId ON "grade" (StudentId);
CREATE INDEX Idx_GradeStudentIdCourseId		-- this is not UNIQUE !!
	ON "grade" (StudentId, CourseId);

CREATE INDEX Idx_equivalentOldCourseId ON "equivalent" (OldCourseId);
CREATE INDEX Idx_equivalentNewCourseId ON "equivalent" (NewCourseId);
CREATE UNIQUE INDEX Idx_equivalentOldCourseIdNewCourseId
	ON "equivalent" (OldCourseId, NewCourseId);

COMMIT;
