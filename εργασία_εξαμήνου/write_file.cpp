// Αποθήκευση δεδομένων εφαρμογής σε αρχεία CSV

#include "write_file.hpp"
using namespace std;




// Γράψε δεδομένα text (τύπου strng) σε ένα αρχείο CSV, το οποίο επιλέγεται
// ανάλογα με την παράμετρο action

void write_file(int action, strng text) {
  strng filename, msg;
  ofstream outfile;

  try {

    // επιλογή ποιο αρχείο θα ανοιχθεί
    switch (action) {
    case DFerror:
      filename = OUT_errors;
      break;

    case DFwarning:
      filename = OUT_warnings;
      break;

    default:
      throw Exairesi(EXC_fatal, "Κλήση της write_csv_file με άγνωστο τύπο ενέργειας!");
    }

    outfile.open(filename.c_str(), ios_base::app);	// apppend to file
    if (!(outfile.good())) {
      msg = "Αποτυχία ανοίγματος αρχείου (";
      msg += filename;
      msg += ") για εγγραφή";
      throw Exairesi(EXC_fatal, msg);
    }

    // εγγραφή της πληροφορίας στο αρχείο
    outfile << text << endl;
    if (!(outfile.good())) {
      msg = "Αποτυχία εγγραφής στο αρχείο (";
      msg += filename;
      msg += ")";
      throw Exairesi(EXC_fatal, msg);
    }

    outfile.close();
    if (!(outfile.good())) {
      msg = "Αποτυχία κλεισίματος αρχείου (";
      msg += filename;
      msg += ")";
      throw Exairesi(EXC_fatal, msg);
    }

  }
  catch (Exairesi &e) { }	/* γνωστή εξαίρεση; */
  catch (...) { Exairesi(); }	/* γενική εξαίρεση; */

}
