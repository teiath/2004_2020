// Χειρισμός αντικειμένων της application

#include <string.h>			// strncpy()
#include <stdio.h>			// sprintf(), sscanf()
#include "application.hpp"
#include "mapOps.hpp"
#include "student.hpp"
#include "write_file.hpp"
#include "common.hpp"
using namespace std;

map<strng, Student&>& Application::get_map_student() {
  return mapS;
}
map<strng, strng>& Application::get_map_course() {
  return mapC;
}
map<strng, strng>& Application::get_map_equivalent_old_new() {
  return mapE;
}
map<strng, float>& Application::get_map_grade() {
  return mapG;
}


// μορφοποίηση μηνύματος με στατιστικά
strng Application::infoMsg(int read_or_write, strng f, long long int n) {
  strng info;
  strng str = f;
  info = (read_or_write == 0 ? "Διαβάστηκ" : "Γράφτηκ" );
  info += (n==1) ? "ε\t" : "αν\t";
  info += (strng) n;
  info += (n==1) ? "\t εγγραφή " : "\t εγγραφές";
  info += " από ";
  info += str;
  info += "\n";
  return info;
}




// υπάρχει παλαιό μάθημα με κωδικό s;
bool Application::exists_old_course_with_code(const strng &s) {
  return
    (get_map_course().find(s) != get_map_course().end()) &&
    (get_map_equivalent_old_new().find(s) != get_map_equivalent_old_new().end()) ;
}


// υπάρχει νέο μάθημα με κωδικό s;
bool Application::exists_new_course_with_code(const strng &s) {
  return
    (get_map_course().find(s) != get_map_course().end()) &&
    (! (exists_old_course_with_code(s))) ;
}


// ποιος ο κωδικός του νέου μαθήματος που αντιστοιχεί στο παλαιό με κωδικό s;
strng Application::equivalent_course(const strng &s) {
  strng ret = "";
  if (!(exists_old_course_with_code(s))) return ret;

  auto it = get_map_equivalent_old_new().find(s);
  ret = (*it).second;
  return ret;
}


// ποιο το όνομα του μαθήματος με κωδικό s;
strng & Application::name_of_course(const strng & s) {
  auto sp = new strng;
  *sp = "";
  auto i = get_map_course().find(s);
  if (i != get_map_course().end()) *sp = i->second;
  return *sp;
}


void output_warning(strng m1, strng m2, strng m3, strng m4) {
  strng msg  = m1;
  msg += CSV_delimiter;
  msg += m2;
  msg += CSV_delimiter;
  msg += m3;
  msg += CSV_delimiter;
  msg += m4;
  write_file(DFwarning, msg);
}



void output_error(strng m1, strng m2, strng m3, strng m4,
                  strng m5, strng m6, strng m7, strng m8) {
  strng msg  = m1;		// student code
  msg += CSV_delimiter;
  msg += m2;			// student last name
  msg += CSV_delimiter;
  msg += m3;			// student last name
  msg += CSV_delimiter;
  msg += m4;			// old course code
  msg += CSV_delimiter;
  msg += m5;			// old course name
  msg += CSV_delimiter;
  msg += m6;			// new course code
  msg += CSV_delimiter;
  msg += m7;			// new course name
  msg += CSV_delimiter;
  msg += m8;			// old grade
  write_file(DFerror, msg);
}

// Προσθήκη αντικειμένων στις δομές δεδομένων του συστήματος


/* ΦΟΙΤΗΤΕΣ */
bool Application::add_student(strng student_code,strng first_name, strng last_name) {
  auto s = new Student;
  s->set(first_name, last_name);
  strng msg;
  bool ignore_this = false;

  if ((!ignore_this) && (student_code=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΚΩΔΙΚΟΣ ΦΟΙΤΗΤΗ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (first_name=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΟΝΟΜΑ ΦΟΙΤΗΤΗ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (last_name=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΕΠΩΝΥΜΟ ΦΟΙΤΗΤΗ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if ((!ignore_this) && (get_map_student().find(student_code) != get_map_student().end())) {
    msg = "ΠΟΛΛΑΠΛΗ ΕΓΓΡΑΦΗ ΓΙΑ ΦΟΙΤΗΤΗ ΜΕ ΤΟΝ ΙΔΙΟ ΚΩΔΙΚΟ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if (ignore_this) {
    output_warning(msg, student_code, last_name, first_name);
    return true;
  }

  std::pair<map<strng, Student&>::iterator,bool> ret;
  ret = get_map_student().insert(pair<strng, Student&>(student_code, *s));
  return ret.second;

}








/* ΜΑΘΗΜΑΤΑ */
bool Application::add_course(strng course_code,strng course_name) {
  strng msg;
  bool ignore_this = false;

  if ((!ignore_this) && (course_code=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΚΩΔΙΚΟΣ ΜΑΘΗΜΑΤΟΣ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (course_name=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΟΝΟΜΑ ΜΑΘΗΜΑΤΟΣ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if ((!ignore_this) && (get_map_course().find(course_code) != get_map_course().end())) {
    msg = "ΠΟΛΛΑΠΛΗ ΕΓΓΡΑΦΗ ΓΙΑ ΜΑΘΗΜΑ ΜΕ ΤΟΝ ΙΔΙΟ ΚΩΔΙΚΟ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if (ignore_this) {
    output_warning(msg, course_code, course_name, "");
    return true;
  }

  std::pair<map<strng, strng>::iterator,bool> ret;
  ret = get_map_course().insert(pair<strng, strng>(course_code,course_name));
  return ret.second;

}







/* ΙΣΟΔΥΝΑΜΙΕΣ ΜΑΘΗΜΑΤΩΝ */
bool Application::add_equivalent_old_new(strng old_course, strng new_course) {
  strng msg;
  bool ignore_this = false;

  if ((!ignore_this) && ((old_course=="")||(new_course==""))) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΚΩΔΙΚΟΣ ΜΑΘΗΜΑΤΟΣ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if ((!ignore_this) && (
    (get_map_course().find(old_course) == get_map_course().end())
    ||
    (get_map_course().find(new_course) == get_map_course().end())
  )) {
    msg = "ΙΣΟΔΥΝΑΜΙΑ ΓΙΑ ΑΓΝΩΣΤΟ ΜΑΘΗΜΑ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if ((!ignore_this) && (
    (get_map_equivalent_old_new().find(old_course) != get_map_equivalent_old_new().end())
  )) {
    msg = "ΠΟΛΛΑΠΛΗ ΕΓΓΡΑΦΗ ΙΣΟΔΥΝΑΜΙΑΣ ΜΑΘΗΜΑΤΩΝ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if (ignore_this) {
    output_warning(msg, new_course, old_course, "");
    return true;
  }


  std::pair<map<strng, strng>::iterator,bool> ret;
  ret = get_map_equivalent_old_new().insert(pair<strng, strng>(old_course, new_course));
  return ret.second;
}









/* ΒΑΘΜΟΙ */
bool Application::add_grade(strng student_code, strng course_code, strng sgrade) {

  strng msg;
  float grade;
  bool ignore_this = false;

  // αλλαγή της υποδιαστολής σε τελεία για τον βαθμό (sgrade)
  char *tmp = new char[16];
  strncpy (tmp, sgrade.c_str(), (sizeof tmp)/(sizeof(char)));
  tmp[(sizeof tmp)/(sizeof(char))] = (char)0;
  char *cp = tmp;
  char *np = cp;
  while (*cp) {
    if (*cp==',') *cp='.';
    cp++;
  }
  // μετατροπή του sgrade σε float
  sscanf(np, "%f", &grade);

  if ((!ignore_this) && (student_code=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΚΩΔΙΚΟΣ ΦΟΙΤΗΤΗ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (course_code=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΚΩΔΙΚΟΣ ΜΑΘΗΜΑΤΟΣ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (sgrade=="")) {
    msg = "ΚΕΝΟ ΠΕΔΙΟ ΒΑΘΜΟΣ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && ((grade <0) || (grade >10))) {
    msg = "ΒΑΘΜΟΣ ΕΚΤΟΣ ΤΟΥ ΔΙΑΣΤΗΜΑΤΟΣ [0,10] (θα αγνοηθεί)";
    ignore_this = true;
  }

  if ((!ignore_this) && (get_map_student().find(student_code) == get_map_student().end())) {
    msg = "ΒΑΘΜΟΣ ΓΙΑ ΑΓΝΩΣΤΟ ΦΟΙΤΗΤΗ (θα αγνοηθεί)";
    ignore_this = true;
  }
  if ((!ignore_this) && (get_map_course().find(course_code) == get_map_course().end())) {
    msg = "ΒΑΘΜΟΣ ΓΙΑ ΑΓΝΩΣΤΟ ΜΑΘΗΜΑ (θα αγνοηθεί)";
    ignore_this = true;
  }

  if (ignore_this) {
    output_warning(msg, student_code, course_code, sgrade);
    return true;
  }


  // Ως κλειδί χρησιμοποιείται το ζεύγος κωδικός φοιτητή:κωδικός μαθήματος
  strng key = student_code;
  key += CSV_delimiter;
  key += course_code;


  std::pair<map<strng, float>::iterator,bool> ret;
  float old_grade;


  /*
      Αποθήκευσε τον βαθμό που διαβάστηκε αν δεν υπάρχει.
        Αν υπάρχει βαθμός μεγαλύτερος >=5 και ο νυν <5, σημείωσέ το.
        Αν υπάρχει βαθμός μεγαλύτερος ή ίσος, αγνόησε τον τρέχοντα.
        Αν υπάρχει βαθμός μικρότερος, άλλαξέ τον με τον τρέχοντα (βελτίωση).
  */
  ret = get_map_grade().insert(pair<strng, float>(key, grade));
  if (ret.second) {			// δεν υπήρχε ήδη βαθμός, αποθηκεύτηκε
    return ret.second;
  }

  old_grade = ret.first->second;	// υπήρχε βαθμός


  if ((grade < 5) && (old_grade >= 5)) {
    output_warning("ΒΑΘΜΟΣ <5 ΕΝΩ ΥΠΑΡΧΕΙ ΗΔΗ >=5 (θα αγνοηθεί)",
                   student_code, course_code, sgrade);
    return true;
  }

  if (grade <= old_grade) {
    /*
      output_warning("ΥΠΑΡΧΕΙ ΗΔΗ ΒΑΘΜΟΣ ΙΣΟΣ Ή ΜΕΓΑΛΥΤΕΡΟΣ (θα αγνοηθεί)",
                     student_code, course_code, sgrade);
    */
    return true;
  }


  /*  βελτίωση βαθμού */
  return (
           (get_map_grade().erase(key)==1) &&
           get_map_grade().insert(pair<strng, float>(key, grade)).second
         );

}









/* ΒΑΘΜΟΙ ΠΑΛΑΙΩΝ ΜΑΘΗΜΑΤΩΝ ΠΟΥ ΔΕΝ ΥΠΑΡΧΟΥΝ ΣΤΟ ΑΝΤΙΣΤΟΙΧΟ ΝΕΟ */
pair<long long int,long long int> Application::missing_grades() {

  long long int missing = 0, non_matching = 0;
  bool ignore_this = false;
  strng msg;
  strng student_code, old_course_code, new_course_code;
  float old_grade = 0, new_grade = 0;
  strng student_old_course_key, student_new_course_key;
  strng student_first_name, student_last_name;
  strng old_course_name, new_course_name;


  // Ψάξε σε όλον τον πίνακα βαθμολογιών
  for (auto it_old_grade = get_map_grade().begin() ;
       it_old_grade != get_map_grade().end(); it_old_grade++) {

    // το κλειδί είναι το ζεύγος   student_code : course_code
    student_old_course_key = it_old_grade->first;

    // βρες τα αντίστοιχα τμήματα  student_code και course_code
    char *cp = new char[student_old_course_key.size()+1];
    strncpy(cp, student_old_course_key.c_str(), student_old_course_key.size()+1);
    cp[student_old_course_key.size()] = (char) 0;
    const char *cbegin = cp;

    bool found_old_course_code = false;
    while (*cp) {
      if (*cp != CSV_delimiter) {
        cp++;
      } else {
        found_old_course_code = true;
        *cp = (char) 0;
        student_code = cbegin;
        cp++;
        old_course_code = cp;
        break;
      }
    }


    // αν ο βαθμός είναι για μάθημα του παλαιού προγράμματος σπουδών
    if (found_old_course_code) {

      // ποιος είναι τα στοιχεία του φοιτητή με κωδικό student_code;
      auto it_student = get_map_student().find(student_code);
      student_first_name = it_student->second.get_first();
      student_last_name = it_student->second.get_last();


      // ποιος είναι ο κωδικός του αντίστοιχου νέου μαθήματος
      auto it_equivalent = get_map_equivalent_old_new().find(old_course_code);
      if (it_equivalent != get_map_equivalent_old_new().end()) {

        new_course_code = it_equivalent->second;
        old_course_name = name_of_course(old_course_code);
        new_course_name = name_of_course(new_course_code);
        old_grade = it_old_grade->second;
        // αλλαγή της τελείας σε υποδιαστολή για τον βαθμό (old_grade)
        auto tmp = new char[16];
        snprintf(tmp,(sizeof tmp/sizeof tmp[0]),"%6f", old_grade);
        char *cp = &tmp[0];
        char *np = cp;
        while (*cp) {
          if (*cp=='.') *cp=',';
          cp++;
        }
        strng old_grade_as_strng = np;

        // Υπάρχει βαθμός στο αντίστοιχο νέο μάθημα;
        student_new_course_key = student_code;
        student_new_course_key += CSV_delimiter;
        student_new_course_key += new_course_code;

        auto it_new_grade = get_map_grade().find( student_new_course_key);
        if (it_new_grade == get_map_grade().end()) {

          // δεν βρέθηκε βαθμός στο αντίστοιχο νέο μάθημα, σημείωσέ την έλλειψη
          missing++;
          output_error(
            student_code, student_last_name, student_first_name,
            old_course_code, old_course_name,
            new_course_code, new_course_name,
            old_grade_as_strng);

        } else {

          new_grade = it_new_grade->second;

          if (old_grade != new_grade) {

            // βρέθηκε κάποιος βαθμός στο αντίστοιχο νέο μάθημα, αλλά
            // παλαιός και νέος βαθμός δεν συμπίπτουν, σημείωσέ το
            non_matching++;
            output_warning(
              "ΒΑΘΜΟΣ ΣΤΟ ΝΕΟ ΜΑΘΗΜΑ ΔΙΑΦΟΡΕΤΙΚΟΣ ΑΠΟ ΤΟ ΠΑΛΑΙΟ (να ελεγχθεί)",
              student_code, new_course_code, old_grade_as_strng);
          }
        }
      }


    }
    delete []cbegin;
  }

  return pair<long long int, long long int>(missing, non_matching);
}


