// Διαχείριση αντικειμένων τύπου T σε δομές map με κλειδί τύπου strng

#ifndef __MAPOPS__
#define __MAPOPS__

#include <map>
#include "strng.hpp"
using namespace std;

template <typename T>
bool map_insert(map<strng, T&> &m, strng s, T &t);

template <typename T>
bool map_delete(map<strng, T&> &m, const strng s);

template <typename T>
bool map_exists(map<strng, T&> &m, const strng s);

#endif		// MAPOPS
