#include "student.hpp"

strng Student::get_first()const {
  return *(first_last.first);
}
strng Student::get_last()const {
  return *(first_last.second);
}

void Student::set(const strng f, const strng l) {
  auto t = new pair<strng *,strng *>;
  auto fp = new strng;
  auto lp = new strng;
  *fp = f;
  *lp = l;
  *t = make_pair(fp, lp);
  first_last = *t;
}

Student::Student() {
  auto t = new pair<strng *,strng *>;
  auto fp = new strng;
  auto lp = new strng;
  fp->set("");
  lp->set("");
  *t = make_pair(fp, lp);
  first_last = *t;
};

Student::~Student() {
  if (first_last.first)  delete first_last.first;
  if (first_last.second) delete first_last.second;
};

Student::Student(const strng f, const strng l) {
  auto t = new pair<strng*,strng*>;
  auto fp = new strng;
  auto lp = new strng;
  *fp = f;
  *lp = l;
  *t = make_pair(fp, lp);
  first_last = *t;
};

Student & Student::operator=(const Student & s) {
  if (this == &s)
    return *this;

  if (first_last.first  != NULL) delete first_last.first;
  if (first_last.second != NULL) delete first_last.second;

  auto t = new pair<strng*,strng*>;
  auto fp = new strng;
  auto lp = new strng;
  *fp = s.get_first();
  *lp = s.get_last();
  *t = make_pair(fp, lp);
  first_last = *t;
};


bool Student::operator==(const Student & s) const {
  if (s.get_first() != (*this).get_first()) return false;
  if (s.get_last() != (*this).get_last()) return false;
  return true;
}

bool Student::operator!=(const Student & s) const {
  return (!(*this == s));
};


bool Student::operator<(const Student& s) const {
  // Ταξινόμηση βάσει επωνύμου πρώτα και ονόματος μετά
  if ((get_last() == s.get_last())&&(get_first() == s.get_first()))return false;
  if ((get_last() == s.get_last())&&(get_first() < s.get_first())) return true;
  return (get_last() < s.get_last());
}

ostream & operator<<(ostream & o, const Student &s) {
  o << s.get_last().c_str() ;
  o << CSV_delimiter ;
  o << s.get_first().c_str();
  return o;
}





#ifdef TEST_UNIT

#include <map>
#include <iostream>
using namespace std;

int main() {

  map <strng, Student&> M;

  Student X, Y;
  cout << "X, Y" << endl;
  X = Y;


  strng f, l;
  f = "ΚΑΛΛΙΟΠΗ";
  l = "ΗΛΙΑΔΗ";
  Student A(f, l);
//cout << "A ??" << endl;
  cout << "first: "<< A.get_first() << endl;
  cout << "last : "<< A.get_last() << endl;
  cout << "A: " << A << endl;;
  l = "ΗΛΙΑΔΗΣ";
  Student B("ΜΗΝΑΣ", l);
  cout << "B: " << B << endl;

  Student C("ΗΛΙΑΣ", "ΠΑΠΑΔΟΠΟΥΛΟΣ");
  cout << "C: " << C << endl;
  cout << "first: "<< C.get_first() << endl;
  cout << "last : "<< C.get_last() << endl;
  Student D("ΜΑΡΙΟΣ", "ΚΩΒΑΙΟΣ");
  cout << "D: " << D << endl;
  Student E("ΖΗΣΗΣ", "ΚΩΒΑΙΟΣ");
  cout << "E: " << E << endl;

  Student F;
  F.set("ΖΑΧΑΡΙΑΣ", "ΚΩΒΑΙΟΣ");
  cout << "F: " << F << endl;
  cout << "E == C? " << ( E==C ) << "\tE != C? " << ( E!=C ) <<"\n";
  cout << "E <  C? " << ( E< C ) << "\tC <  E? " << ( C<E ) <<"\n";
  cout << "E == F? " << ( E==F ) << "\tE != F? " << ( E!=F ) <<"\n";
  cout << "E <  F? " << ( E< F ) << "\tF <  E? " << ( F<E) <<"\n";

  Student *p = new Student;
  *p = E;
  cout << "*p: " << *p << endl;
  delete p;
  Student *q = new Student;
  *q = F;
  cout << "*q: " << *q << endl;



  M.insert(pair<strng,Student&>("A", A));
  M.insert(pair<strng,Student&>("B", B));
  M.insert(pair<strng,Student&>("C", C));
  M.insert(pair<strng,Student&>("D", D));
  M.insert(pair<strng,Student&>("E", E));
  M.insert(pair<strng,Student&>("F", F));

  for (auto it=M.begin(); it!=M.end(); it++) {
    cout << it->first << ">" << it->second << endl;
  }
}

#endif	//TEST_UNIT
