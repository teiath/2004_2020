#ifndef __APPLICATION__
#define __APPLICATION__
#include <map>

// Κλάση για το αντικείμενο strng
#include "strng.hpp"

// Κλάση για το αντικείμενο φοιτητής (Student)
#include "student.hpp"


class Application {

//
// Ιδιωτικές δομές δεδομένων
//
private:
  // δομή δεδομένων για φοιτητές
  // (κωδικός     στοιχεία φοιτητή)
  map<strng, Student&> mapS;

  // δομή δεδομένων για μαθήματα
  // (κωδικός     λεκτικό μαθήματος)
  map<strng, strng> mapC;

  // δομή δεδομένων για ισοδυναμίες μαθημάτων
  // (κωδικός παλαιού μαθήματος      κωδικός ισοδύναμου νέου μαθήματος)
  map<strng, strng> mapE;

  // δομή δεδομένων για βαθμούς
  // (κωδικός φοιτητή+κωδικός μαθήματος       βαθμός εξέτασης)
  map<strng, float> mapG;


//
// Δημόσιες μέθοδοι χειρισμού των αντικειμένων
//
public:
  map<strng, Student&>& get_map_student();
  map<strng, strng>& get_map_course();
  map<strng, strng>& get_map_equivalent_old_new();
  map<strng, float>& get_map_grade();

  // Προσθήκη δεδομένων στις δομές της μνήμης
  bool add_student(strng student_code,strng first_name, strng last_name);
  bool add_course(strng course_code,strng course_name);
  bool add_equivalent_old_new(strng old_course, strng new_course);
  bool add_grade(strng student_code, strng course_code, strng grade);


  // Φόρτωμα όλων των δεδομένων της εφαρμογής στην μνήμη από αρχεία CSV
  strng read_files();


  // για ποιους παλαιούς βαθμούς λείπει ή δεν ταιριάζει ο αντίστοιχος νέος;
  pair<long long int, long long int> missing_grades();


  // υπάρχει παλαιό μάθημα με κωδικό s;
  bool exists_old_course_with_code(const strng &s);

  // υπάρχει νέο μάθημα με κωδικό s;
  bool exists_new_course_with_code(const strng &s);

  // ποιος ο κωδικός του νέου μαθήματος που αντιστοιχεί στο παλαιό με κωδικό s;
  strng equivalent_course(const strng &s);

  // ποιο το όνομα του μαθήματος με κωδικό s;
  strng & name_of_course(const strng &s);



  // Ανάγνωση δεδομένων από αρχείο CSV
  long long int read_csv_file(int action, strng filename, int nfields,
                              int ignore_lines, char inp_delimiter);
  // Εγγραφή δεδομένων σε αρχείο CSV
  void write_csv_file(int action, strng text);


  // Μορφοποίηση μηνύματος με στατιστικά
  strng infoMsg(int read_or_write, strng f, long long int n);

};


#endif
