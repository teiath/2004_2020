#include <string.h>		// strstr(),strlen()
#include <map>
#include "strng.hpp"
#include "exairesi.hpp"
#include "application.hpp"
#include "common.hpp"

strng Application::read_files() {
  strng info;

  /* ΦΟΙΤΗΤΕΣ */
  info +=
    infoMsg(0, INP_student,
            read_csv_file(DFstudent, INP_student, 3, 1, CSV_delimiter));

  /* ΜΑΘΗΜΑΤΑ */
  info +=
    infoMsg(0, INP_course,
            read_csv_file(DFcourse, INP_course, 2, 1, CSV_delimiter));

  /* ΙΣΟΔΥΝΑΜΙΕΣ ΜΑΘΗΜΑΤΩΝ */
  info +=
    infoMsg(0, INP_equivalent,
            read_csv_file(DFequivalent, INP_equivalent, 2, 1, CSV_delimiter));

  /* ΒΑΘΜΟΙ */
  info +=
    infoMsg(0, INP_grade,
            read_csv_file(DFgrade, INP_grade, 3, 1, CSV_delimiter));

  return info;
}

// Διάβασε τα πρώτα nfields πεδία από ένα αρχείο CSV με όνομα filename.

// Τα πεδία διαχωρίζονται από τον χαρακτήρα inp_delimiter,
// ενώ αγνοούνται οι πρώτες ignore_lines του αρχείου
// (η πρώτη γραμμή λ.χ. μπορεί να περιέχει τα ονόματα των πεδίων)

// Για κάθε γραμμή που διαβάζεται εκτελείται αντίστοιχη προσθήκη των
// δεδομένων που διαβάστηκαν σε μια δομή δεδομένων μνήμης, η οποία
// επιλέγεται βάσει του ορίσματος action

// Επιστρέφει τον αριθμό των γραμμών δεδομένων που διαβάστηκαν επιτυχώς

// Σε περίπτωση λάθους προκαλεί εξαίρεση



#include <stdio.h> 		// fopen(), getline(), fclose()
#include "exairesi.hpp"

long long int Application::read_csv_file(int action, strng filename,
    int nfields, int ignore_lines, char inp_delimiter) {
  int i;
  char *cp;

  char **field_start = new char* [nfields]; // η θέση αφετηρίας του κάθε πεδίου
  char *linebuf = NULL;
  size_t linebuf_size = 0;
  long long int lines_read_so_far = 0, data_lines_read = 0;
  ssize_t linesize;
  char lineNo[30];		// για μετατροπή του αριθμού γραμμής σε C string
  char *lineNop = lineNo;
  FILE *fp;
  strng msg;

  try {

    fp = fopen(filename.c_str(), "r");
    if (fp==NULL) {
      msg = "Αδυναμία ανοίγματος του αρχείου ";
      msg += filename;
      throw Exairesi(EXC_fatal, msg);
    }



    linesize = getline(&linebuf, &linebuf_size, fp);
    if (linesize == (-1)) {
      snprintf(lineNop, sizeof(lineNo)/sizeof (lineNo[0]), "%lld", lines_read_so_far);
      strng tmp(lineNop);
      msg = "Αποτυχία διαβάσματος της γραμμής ";
      msg += tmp;
      throw Exairesi(EXC_fatal, msg);
    }

    while (linesize >= 0) {

      lines_read_so_far++;
      if (lines_read_so_far > ignore_lines) {

        for (i = 0; i < nfields; i++) {
          // αρχικώς όλα τα πεδία "ξεκινούν" από τον τελικό χαρακτήρα
          // '\0' της getline(), άρα πρακτικώς είναι κενά πεδία
          field_start[i] = (linebuf+linebuf_size - 1);
        }

        // εντόπισε το σημείο αφετηρίας κάθε πεδίου
        cp = linebuf;
        int at_field_beginning = 1;
        int fields_found_so_far = 0;
        while (*cp) {
          if (at_field_beginning == 1) {
            if (fields_found_so_far < nfields) {
              // Η γραμμή που διαβάσαμε μπορεί να έχει περισσότερα πεδία
              // από όσα χρειαζόμαστε.  Αγνόησε τα περισσά.
              field_start[fields_found_so_far] = cp;
            }
            at_field_beginning = 0;
            fields_found_so_far++;
          }
          if (*cp == inp_delimiter) {
            *cp = (char)0;          // εδώ τελειώνει το τρέχον πεδίο
            at_field_beginning = 1; // και από τον επόμενο χαρακτήρα ξεκινά άλλο
          }
          // αγνόησε τον τελικό newline της getline()
          if (*cp == '\n') {
            *cp =  (char)0;
          }
          cp++;
        }




        // Επεξεργασία των πεδίων της γραμμής που μόλις διαβάστηκαν
        bool read_successful;
        strng s1, s2, s3;
        float g;
        char *gp;

        switch ( action ) {

        case DFstudent:
          s1 = field_start[0];	// student code
          s2 = field_start[2];	// first name
          s3 = field_start[1];	// last name
          read_successful = add_student(s1, s2, s3);
          break;

        case DFcourse:
          s1 = field_start[0];	// course code
          s2 = field_start[1];	// course name
          read_successful = add_course(s1, s2);
          break;

        case DFequivalent:
          s1 = field_start[0];	// new course code
          s2 = field_start[1];	// old course code
          read_successful = add_equivalent_old_new(s2, s1);
          break;

        case DFgrade:
          s1 = field_start[0];	// student code
          s2 = field_start[1];	// course code
          s3 = field_start[2];	// grade
          read_successful = add_grade(s1, s2, s3);
          break;

        default:
          throw Exairesi(EXC_fatal, "Κλήση της read_csv_file με άγνωστο τύπο ενέργειας!");

        }


        if (! read_successful) {
          strng msg = "Αποτυχία αποθήκευσης στη μνήμη των δεδομένων που διαβάστηκαν από το αρχείο ";
          msg += filename;
          msg += " στην γραμμή ";
          msg += strng(lines_read_so_far);
          throw Exairesi(EXC_fatal, msg);
        }
        data_lines_read++;
      }
      linesize = getline(&linebuf, &linebuf_size, fp);
    }

    free(linebuf);	// Η μνήμη linebuf δεσμεύθηκε δυναμικά
    linebuf = NULL;	// και δεν χρειάζεται πλέον
    fclose(fp);

  }
  catch (Exairesi &e) { }	/* γνωστή εξαίρεση; */
  catch (...) { Exairesi(); }	/* γενική εξαίρεση; */

  return data_lines_read;
}


#ifdef TEST_UNIT
#define NFIELDS 2
#define CSV_delimiter	';'

#include <iostream>
using namespace std;

map<strng, strng&> C;

bool add_student(strng s1, strng s2, strng s3) {
  return true;
}
bool add_course(strng s1, strng s2) {
  std::pair<map<strng, strng&>::iterator,bool> ret;
  ret = C.insert(pair<strng, strng&>(s1,s2));
  return ret.second;
}
bool add_equivalent_old_new(strng s1, strng s2) {
  return true;
}
bool add_grade(strng s1, strng s2, strng s3) {
  return true;
}

void show_read_data() {
  auto it = C.begin();
  size_t cnt = 0;
  while (it != C.end()) {
    cout << ++cnt << ". " <<  it->first << CSV_delimiter << it->second << endl ;
    it++;
  }
}


int main(int argc, char *argv[]) {
  Application A;
  if (argc < 2) {			// read data from DFcourse
    printf ("%lld γραμμές διαβάστηκαν\n",
            A.read_csv_file(DFcourse, INP_course, 2, 1, CSV_delimiter)
           );
  } else {				// read data from argv[1]
    printf ("%lld γραμμές διαβάστηκαν\n",
            A.read_csv_file(DFcourse, (strng)argv[1], NFIELDS, 1, CSV_delimiter)
           );
  }


  show_read_data();
  return 0;
}

#endif				//TEST_UNIT
