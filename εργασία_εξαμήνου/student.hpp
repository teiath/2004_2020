#ifndef  __STUDENT__
#define __STUDENT__
using namespace std;

//#include "Student.hpp"
#include <iostream>
#include "strng.hpp"
#include "common.hpp"

class Student {

private:
  std::pair <strng *, strng *> first_last;

public:
  strng get_first()const;
  strng get_last()const;
  void set(const strng f, const strng l);
  Student();

  ~Student();
  Student(const strng f, const strng l);

  Student &operator=(const Student &s);
  bool operator==(const Student &s) const;
  bool operator!=(const Student &s) const;
  bool operator<(const Student &s) const;


  friend ostream & operator<<(ostream & o, const Student &s);
};

#endif		// __STUDENT__
