// Κλάση διαχείρισης εξαιρέσεων

#ifndef __EXAIRESI__
#define __EXAIRESI__

enum exception_type {
   EXC_warning,
   EXC_error,
   EXC_fatal
};

#include "strng.hpp"
using namespace std;

class Exairesi {
public:
  Exairesi();
  ~Exairesi();
  Exairesi(const exception_type t, const strng &s);
};


#endif
