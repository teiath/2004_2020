// Για μεταγλώτισση & εκτέλεση:
//	make clean && make && ./a.out
//
// Εμφανίζει μερικά στατιστικά στο κανάλι εξόδου stderr
// και παράγει αναφορές στα αρχεία report*.csv
//

#include <iomanip>		// setw()
#include "application.hpp"

int main(int argc, char **argv) {

  Application A;


  // Ανάγνωση αρχείων εισόδου και αποθήκευση δεδομένων σε δομές της μνήμης
  strng info_msg = "";
  info_msg += A.read_files();
  cerr << info_msg << endl;

  cerr << "Αποθηκεύθηκαν " << setw(6) << A.get_map_student().size() << " φοιτητές στην μνήμη" << endl;
  cerr << "Αποθηκεύθηκαν " << setw(6) << A.get_map_course().size() << " μαθήματα στην μνήμη"<< endl;
  cerr << "Αποθηκεύθηκαν " << setw(6) << A.get_map_equivalent_old_new().size() << " αντιστοιχίες παλαιών/νέων μαθημάτων στην μνήμη"<< endl;
  cerr << "Αποθηκεύθηκαν " << setw(6) << A.get_map_grade().size() << " τελικοί βαθμοί φοιτητών στην μνήμη"<< endl;
  cerr << endl;


  auto mg = A.missing_grades();
  cerr << "  " << setw(5) << mg.first + mg.second << " βαθμοί παλαιών μαθημάτων πρέπει να ελεγχθούν συνολικά:" << endl;
  cerr << "- " << setw(5) << mg.second << " βαθμοί παλαιών μαθημάτων είναι διαφορετικοί στο αντίστοιχο νέο μάθημα" << endl;
  cerr << "- " << setw(5) << mg.first << " βαθμοί παλαιών μαθημάτων δεν έχουν περαστεί στο αντίστοιχο νέο μάθημα" << endl;

  cerr << endl;



  return 0;
}
