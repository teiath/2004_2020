#include <iostream>
#include "strng.hpp"
#include "exairesi.hpp"
#include "write_file.hpp"


using namespace std;

Exairesi::~Exairesi() { }

Exairesi::Exairesi() {
  cerr << "Σφάλμα: γενική εξαίρεση" << endl;
}


Exairesi::Exairesi(const exception_type t, const strng &s) {

  switch (t) {

  case EXC_fatal:	// αν η εξαίρεση είναι λόγω μοιραίου σφάλματος
    cerr << "Μοιραίο σφάλμα: " << s << endl;
    cerr << "Το πρόγραμμα θα τερματιστεί." << endl;
    exit (1);
    break;

  case EXC_warning:
    cerr << "Ειδοποίηση: " << s << endl;
    break;

  case EXC_error:
    cerr << "Σφάλμα: " << s << endl;
    break;

  default:
    cerr << "Άγνωστο σφάλμα: " << s << endl;

  }
}

#ifdef TEST_UNIT

int main () {

  cerr  << "-- Δοκιμή εξαίρεσης με κείμενο σφάλματος" << endl;
  try {
    throw Exairesi(EXC_warning, "Η δεξαμενή 27 υπερθερμάνθηκε");
    cerr  << endl;
  } catch (Exairesi &e) { }	/* γνωστή εξαίρεση; */
  cerr << endl;


  try {
    cerr << "-- Δοκιμή γενικής εξαίρεσης" << endl;
    throw Exairesi();		/* γενική εξαίρεση; */
  } catch (Exairesi &e) { }	/* γνωστή εξαίρεση; */
  cerr << endl;


  cerr << "-- Δοκιμή μοιραίας εξαίρεσης" << endl;
  try {
    throw Exairesi(EXC_fatal, "ολική απώλεια επαφής με πλανήτη Γη");
  } catch (Exairesi &e) { }	/* γνωστή εξαίρεση; */



  cerr << endl << "Κανονικό τέλος προγράμματος" << endl; // δεν φτάνεται ποτέ
}

#endif				//TEST_UNIT
