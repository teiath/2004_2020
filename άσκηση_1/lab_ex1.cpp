/* ΑΝΤΙΚΕΙΜΕΝΟΣΤΡΕΦΗΣ ΠΡΟΓΡΑΜΜΑΤΙΣΜΟΣ

Άσκηση 1

Από τη C στη C++

1. Κατεβάστε, μεταφράστε και εκτελέστε το πρόγραμμα Ex1.cpp.
Σε περίπτωση που τρέχετε το πρόγραμμα στην Dev C++, μετατρέψτε την
κωδικοποίηση των χαρακτήρων σε “latin” μέσω του notepad++ ή
αντίστοιχου εργαλείου και προσθέστε την εντολή: system (“chcp 1253”);
στην αρχή του προγράμματος. Μελετήστε το και καταγράψτε τις απορίες σας.


2. Γράψτε ένα ενιαίο πρόγραμμα μέσω του οποίου θα επιδεικνύονται τα ακόλουθα:

Η αλλαγή τιμής σε const integer.

H χρήση των τελεστών new, new[], delete και delete[].

Η χρήση της αναφοράς στις παραμέτρους και τον τύπο επιστροφής
των συναρτήσεων (δύο συναρτήσεις)

Τις συναρτήσεις με παραμέτρους οι οποίες έχουν default τιμές. */




#include "iostream"
using namespace std;

#define abs(x)		((x)>=0?(x):(-1)*(x))

void test(int n, string s) {
  cout << "\n\nΔοκιμή: "<< n << " (" << s << ")" << endl;
}

typedef struct {
  float x, y;
} complex;

void complex_print(complex c) {
  cout << "( "<< c.x << ", " << c.y << " )" << endl;
}

void complex_array_print(int n, complex *a) {
  for (int i=0; i<n; i++) {
    cout << "\t" << i << ": ";
    complex_print(a[i]);
  }
}


// Παράδειγμα συναρτήσεως με αναφορά στις παραμέτρους
void complex_double(complex &c) {
  c.x *= 2;
  c.y *= 2;
};



// Παράδειγμα συναρτήσεως με αναφορά στην τιμή που επιστρέφει
complex &complex_3rd_element_of_array(complex *c) {
  return c[2];
}



// Παράδειγμα συναρτήσεως με παραμέτρους οι οποίες έχουν default τιμές
void func_default_arguments(const char *args, int a, int b=7, int c=10) {
  cout << "func_default_arguments(" << args <<
       ")    --->      a: " << a << "  b: " << b << "  c: " << c << "" << endl;
}






int main() {

  int ntests = 0;

  test(++ntests, "αλλαγή τιμής σε const integer");
  int x = 3;
  x = 4;
  cout << "x = " << x << endl; // <--- αυτό δουλεύει κανονικά
#ifdef TEST
  const int y = 3;
  y = 4;         // <--- αυτό προκαλεί σφάλμα μεταγλώτισσης
#endif




  test(++ntests, "χρήση των τελεστών new, new[], delete και delete[]");
  complex c;
  c.x = 19.0;
  c.y = -3.0;
  complex *c1 = new complex;
  c1->x = 4.0;
  c1->y = 5.0;
  cout << "Δημιουρία νέου μιγαδικού c1 με χρήση της new" << endl;
  complex_print(*c1);
  complex *memoryMarker1 = new complex;
  cout << "θέση μνήμης στην οποία αποθηκεύεται ο νέος μιγαδικός: "<< c1 << "" << endl;
  cout << "Ποσότητα μνήμης που δεσμεύτηκε για αυτόν: "
       << abs((long long)c1 -(long long)memoryMarker1) << " bytes" << endl;
  complex *c2 = new complex[3];
  cout << "Δημιουρία νέου τριμελή πίνακα μιγαδικών c3 με χρήση της newp[]" << endl;
  complex *memoryMarker2 = new complex;
  complex_array_print(3, c2);
  cout << "θέση μνήμης στην οποία αρχίζει η αποθήκευση των 3 νέων μιγαδικών "<< c2 << "" << endl;
  cout << "Ποσότητα μνήμης που δεσμεύτηκε για αυτούς: "
       << abs((long long)c2 -(long long)memoryMarker2) << " bytes" << endl;
  delete c1;
  delete []c2;



  test(++ntests, "χρήση της αναφοράς στις παραμέτρους");
// χρήση της αναφοράς στις παραμέτρους και τον τύπο επιστροφής
// των συναρτήσεων (δύο συναρτήσεις)
  cout << "Ένας μιγαδικός:            c = ";
  complex_print( c );		// πέρασμα ορίσματος με τιμή
  complex_double( c );		// πέρασμα ορίσματος με αναφορά
  cout << "Ο ίδιος διπλασιασμένος:    c = ";
  complex_print(c);


  test(++ntests, "χρήση της αναφοράς στον τύπο επιστροφής");
  complex *carray = new complex[3];
  cout << "Ένας πίνακας με 3 μιγαδικούς:" << endl;
  for (int i=0; i<3; i++) {
    carray[i].x = i+1;
    carray[i].y = -2*(i*1);
  };
  complex_array_print(3, carray);
  complex c3_by_value;
  cout << endl << "Ένας νέος μιγαδικός δημιουργείται από το 3ο στοιχείο του πίνακα:" << endl;
  cout << "(χωρίς τη χρήση της αναφοράς στον τύπο επιστροφής)" << endl;
  c3_by_value = complex_3rd_element_of_array(carray);
  cout << "\tc3_by_value = ";
  complex_print(c3_by_value);
  c3_by_value.x = 20;		// αλλαγή τιμής
  cout << "και αλλάζει η τιμή του..." << endl;
  cout << "\tc3_by_value = ";
  complex_print(c3_by_value);
  cout << "Αν και άλλαξε η τιμή του c3, ο πίνακας δεν άλλαξε:" << endl;
  complex_array_print(3, carray);
  cout << endl << "Ένας νέος μιγαδικός δημιουργείται από το 3ο στοιχείο του πίνακα:" << endl;
  cout << "(με χρήση της αναφοράς στον τύπο επιστροφής)" << endl;
  complex &c3_by_reference = complex_3rd_element_of_array(carray);
  cout << "c3_by_reference = ";
  cout << "\tc3_by_reference = ";
  c3_by_reference.x = 20;		// αλλαγή τιμής
  cout << "και αλλάζει η τιμή του..." << endl;
  cout << "\tc3_by_reference = ";
  complex_print(c3_by_reference);
  cout << "Επειδή άλλαξε η τιμή του c3, άλλαξε και ο πίνακας:" << endl;
  complex_array_print(3, carray);



  test(++ntests, "χρήση συναρτήσεως με παραμέτρους οι οποίες έχουν default τιμές");
  func_default_arguments("1, 1, 1",   1, 1, 1);
  func_default_arguments("2, 2   ",   2, 2   );
  func_default_arguments("3      ",   3      );
#ifdef TEST
  // βάσει του ορισμού μόνον οι 2 τελευταίες παράμετροι μπορεί να απουσιάζουν
  func_default_arguments(       );// <--- αυτό προκαλεί σφάλμα μεταγλώτισσης
#endif




  return 0;
}
