/*
ΤΕΛΙΚΗ ΕΞΕΤΑΣΗ Θέμα (A-2).
Να δημιουργηθεί η κλάση Student η οποία θα έχει τα ακόλουθα χαρακτηριστικά: 

ΑΜ:       (int)
Όνομα:    (char *)
Εξάμηνο:  (int)
Ενεργός;: (bool)

Να γραφεί συνάρτηση η οποία δημιουργεί και επιστρέφει μία συλλογή
αντικειμένων της κλάσης Students με δεδομένα τα οποία διαβάζονται από
csv αρχείο. Το όνομα του αρχείου δίνεται ως όρισμα στην συνάρτηση.

Να γραφεί συνάρτηση η οποία θα τυπώνει τον ΑΜ και το Όνομα κάθε
ενεργού φοιτητή που βρίσκεται σε συλλογή της προηγούμενης συνάρτησης.

Να γραφεί main συνάρτηση η οποία θα επιδεικνύει την λειτουργία των
παραπάνω. Η κλάση Student δεν θα έχει public χαρακτηριστικά και θα έχει μόνο
τις απαραίτητες μεθόδους για την υλοποίηση των παραπάνω ζητούμενων.
*/


#include <stdio.h>
#include <string.h>
#include <ostream>
#include <fstream>
#include <iostream>
#include <string>
#include <list>
using namespace std;

class STUD {
private:
 char * name;
 int AM;
 int semester;
 bool active;

public:
 STUD();
 STUD(const int i, const char *s, int sem, bool a);
 ~STUD();
 int get_AM();
 char *get_name();
 bool get_active();

 STUD& operator=(const STUD &m);
};


STUD::STUD(){ AM=0; name=NULL; }
STUD::~STUD(){ if (name) delete[]name; }
STUD::STUD(const int i, const char *s, int sem, bool a){
  AM=i;
  if (s == NULL) {name=NULL; return; }
  char *cp = new char[strlen(s)+1];
  strncpy(cp, s, strlen(s));
  cp[strlen(s)]=(char) 0;
  name=cp;
  semester=sem;
  active=a;
}
int  STUD::get_AM(){ return AM; }
char *STUD::get_name(){
  if (name == NULL) {return NULL; }
  char *cp = new char[strlen(name)+1];
  strncpy(cp, name,strlen(name));
  cp[strlen(name)]=(char) 0;
  return cp;
}
bool STUD::get_active(){return active;}

STUD& STUD::operator=(const STUD &m){
  if (this == &m) return *this;
  if (name) delete[]name;	// ελευθέρωσε την παλιά μνήμη
  AM=m.AM;
  if (m.name == NULL) {name=NULL; return *this; }

  char *cp = new char[strlen(m.name)+1];
  strncpy(cp, m.name, strlen(m.name));
  cp[strlen(m.name)] = (char) 0;
  name = cp;
  semester=m.semester;
  active=m.active;
  return *this;
}



void read_collection(list<STUD *> &L, char *filename){
#define CLINE_LENGTH	1024
 string line;
 char *tmp = new char[CLINE_LENGTH];
 int AM; int Semester; bool Active;
 ifstream inp(filename, ios::in);
 int c = 0, iactive;
 cerr << "Διάβασμα δεδομένων:" << endl;
 while (std::getline(inp, line)) {
    cerr << ++c <<": " << line << endl;
    
    sscanf(line.c_str(), "%d|%[^\t\r\n|]|%d|%d", &AM, tmp, &Semester, &iactive);
    char *Name = new char[strlen(tmp)+1]; strcpy(Name, tmp);
    Active = (iactive == 1);
    STUD *t = new STUD(AM,Name,Semester,Active);
    L.push_back(t);
 }
 inp.close();
}


void print_active(ostream& o, list<STUD *> &L){
 o << "\nΕνεργοί φοιτητές:" << endl;
 for (auto i=L.begin();i!=L.end();i++){
    if ((*i)->get_active())
	o << (*i)->get_AM() << "\t" << (*i)->get_name() << endl;
 }
}

int main(){

list<STUD *> L;
read_collection(L, (char *) "input.csv");

print_active(cout, L);

return 0;
}
